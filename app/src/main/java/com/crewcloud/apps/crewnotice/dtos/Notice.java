package com.crewcloud.apps.crewnotice.dtos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by tunglam on 12/15/16.
 */

public class Notice extends RealmObject implements Serializable {

    @SerializedName("NoticeNo")
    private int noticeNo;

    @SerializedName("Title")
    private String title;

    @SerializedName("previewNotice")
    private String content;

    @SerializedName("CommentCount")
    private int commentCount;

    @SerializedName("ViewCount")
    private String viewCount;

    @SerializedName("RegDate")
    private String regDate;

    @SerializedName("NoticeImg")
    private String noticeImg;

    @SerializedName("IsImportant")
    private boolean isImportant;

    @SerializedName("IsAttack")
    private boolean isAttack;

    @SerializedName("IsShare")
    private boolean isShare;

    public Notice() {
    }

    public int getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(int noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getNoticeImg() {
        return noticeImg;
    }

    public void setNoticeImg(String noticeImg) {
        this.noticeImg = noticeImg;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }

    public boolean isAttack() {
        return isAttack;
    }

    public void setAttack(boolean attack) {
        isAttack = attack;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }
}