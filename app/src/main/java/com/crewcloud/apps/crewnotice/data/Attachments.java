package com.crewcloud.apps.crewnotice.data;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by tunglam on 12/26/16.
 */

public class Attachments extends RealmObject implements Serializable {
    @SerializedName("AttachNo")
    private int attachNo;

    @SerializedName("NoticeNo")
    private int noticeNo;

    @SerializedName("FileName")
    private String fileName;

    @SerializedName("DownloadUrl")
    private String downloadUrl;

    @SerializedName("FileLength")
    private int filelength;

    @SerializedName("FilePath")
    private String filepath;

    public Attachments() {
    }

    public Attachments(String fileName, String downloadUrl) {
        this.fileName = fileName;
        this.downloadUrl = downloadUrl;
    }

    protected Attachments(Parcel in) {
        attachNo = in.readInt();
        noticeNo = in.readInt();
        fileName = in.readString();
        downloadUrl = in.readString();
        filelength = in.readInt();
        filepath = in.readString();
    }


    public int getAttachNo() {
        return attachNo;
    }

    public void setAttachNo(int attachNo) {
        this.attachNo = attachNo;
    }

    public int getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(int noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getFilelength() {
        return filelength;
    }

    public void setFilelength(int filelength) {
        this.filelength = filelength;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

}
