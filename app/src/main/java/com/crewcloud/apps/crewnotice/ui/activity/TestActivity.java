package com.crewcloud.apps.crewnotice.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.adapter.PhotoAdapter;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseEvent;
import com.crewcloud.apps.crewnotice.data.Attachments;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;
import com.crewcloud.apps.crewnotice.event.MenuEvent;
import com.crewcloud.apps.crewnotice.factory.DataFactory;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.view.MessageWebView;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dazone on 2/6/2017.
 */

public class TestActivity extends BaseActivity {

    @Bind(R.id.fragment_notice_detail_lv_attach)
    RecyclerView lvAttach;

    @Bind(R.id.fragment_notice_detail_tv_author)
    TextView tvAuthor;

    @Bind(R.id.fragment_notice_detail_tv_des)
    MessageWebView tvDes;

    @Bind(R.id.fragment_notice_detail_tv_time)
    TextView tvTime;

    @Bind(R.id.fragment_notice_detail_tv_title)
    TextView tvTitle;

    PhotoAdapter adapter;

    private NoticeDetail noticeDetail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_test);
        ButterKnife.bind(this);
        lvAttach.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter = new PhotoAdapter(this);
//        noticeDetail = DataFactory.getDetail();

        showData();
    }


    private void showData() {
        if (noticeDetail == null) return;
        if (!TextUtils.isEmpty(noticeDetail.getTitle())) {
            tvTitle.setText(Html.fromHtml(noticeDetail.getTitle()));
        } else {
            tvTitle.setText("");
        }
        tvTime.setText(TimeUtils.getTime(noticeDetail.getStartDate()) + " ~ " + TimeUtils.getTime(noticeDetail.getEndDate()));
        adapter.clear();
        lvAttach.setAdapter(adapter);
        adapter.addAll(noticeDetail.getAttaches());
        tvAuthor.setText(noticeDetail.getUserName() + "( " + noticeDetail.getPositionName() + ",/" + noticeDetail.getDepartName() + " )");
        adapter.setOnClickItemAttach(new PhotoAdapter.ItemClickListener() {
            @Override
            public void onItemClichAttach(int position) {
                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.VIEW_ATTACH);
                Bundle data = new Bundle();
                List<Attachments> list = adapter.getItems();
                data.putString("NAME", noticeDetail.getUserName());
                data.putString("TIME", noticeDetail.getModDate());
                data.putInt("POSITION", position);
                data.putSerializable("LIST_ATTACH", (Serializable) list);
                MenuEvent event = new MenuEvent();
                event.setBundle(data);
                baseEvent.setMenuEvent(event);
                EventBus.getDefault().post(baseEvent);
            }
        });
        initWebContent();


    }


    private void initWebContent() {
        tvDes.configure();
        if (!TextUtils.isEmpty(noticeDetail.getContent())) {

            String content = noticeDetail.getContent();
            tvDes.setFitsSystemWindows(true);
            tvDes.setText(content);
        } else {
            tvDes.setVisibility(View.GONE);
        }
//            tvDes.setWebViewClient(new WebViewClient());
//            tvDes.getSettings().setJavaScriptEnabled(true);
//            tvDes.getSettings().setBuiltInZoomControls(true);
//            tvDes.getSettings().setLoadWithOverviewMode(true);
//            tvDes.getSettings().setDisplayZoomControls(true);
//            String htmlString = noticeDetail.getContent();
//            String summary = "<html><body>" + htmlString + "</body></html>";
//            tvDes.loadData(summary, "text/html; charset=utf-8", "utf-8");
    }

}
