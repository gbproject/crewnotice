package com.crewcloud.apps.crewnotice.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by tunglam on 12/23/16.
 */

public class LeftMenu extends RealmObject implements Parcelable {
    @SerializedName("DivisionNo")
    private int divisionNo;

    @SerializedName("RegUserNo")
    private int regUserNo;

    @SerializedName("RegDate")
    private String regDate;

    @SerializedName("ModUserNo")
    private int modUserNo;

    @SerializedName("ModDate")
    private String modDate;

    @SerializedName("Name")
    private String name;

    @SerializedName("ViewMode")
    private int viewMode;

    public LeftMenu() {
    }


    protected LeftMenu(Parcel in) {
        divisionNo = in.readInt();
        regUserNo = in.readInt();
        regDate = in.readString();
        modUserNo = in.readInt();
        modDate = in.readString();
        name = in.readString();
        viewMode = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(divisionNo);
        dest.writeInt(regUserNo);
        dest.writeString(regDate);
        dest.writeInt(modUserNo);
        dest.writeString(modDate);
        dest.writeString(name);
        dest.writeInt(viewMode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LeftMenu> CREATOR = new Creator<LeftMenu>() {
        @Override
        public LeftMenu createFromParcel(Parcel in) {
            return new LeftMenu(in);
        }

        @Override
        public LeftMenu[] newArray(int size) {
            return new LeftMenu[size];
        }
    };

    public int getViewMode() {
        return viewMode;
    }

    public void setViewMode(int viewMode) {
        this.viewMode = viewMode;
    }

    public int getDivisionNo() {
        return divisionNo;
    }

    public void setDivisionNo(int divisionNo) {
        this.divisionNo = divisionNo;
    }

    public int getRegUserNo() {
        return regUserNo;
    }

    public void setRegUserNo(int regUserNo) {
        this.regUserNo = regUserNo;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public int getModUserNo() {
        return modUserNo;
    }

    public void setModUserNo(int modUserNo) {
        this.modUserNo = modUserNo;
    }

    public String getModDate() {
        return modDate;
    }

    public void setModDate(String modDate) {
        this.modDate = modDate;
    }

    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }


}
