package com.crewcloud.apps.crewnotice.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.adapter.ViewImageAdapter;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.data.Attachments;
import com.crewcloud.apps.crewnotice.dialog.DownloadAttachDialog;
import com.crewcloud.apps.crewnotice.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Dazone on 1/11/2017.
 */

public class ViewAttachActivity extends BaseActivity {

    @Bind(R.id.view_pager)
    ViewPager viewPager;

    @Bind(R.id.img_avatar)
    CircleImageView ivAvatar;

    @Bind(R.id.tv_username)
    TextView tvUserName;

    @Bind(R.id.tv_date)
    TextView tvDate;

    ViewImageAdapter adapter;

    private List<Attachments> listData = new ArrayList<>();
    private int position = 0;
    private String userName, time;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attach);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_80_color));
        }
        Bundle data = getIntent().getBundleExtra("EXTRA");
        if (data != null) {
            listData = (List<Attachments>) data.getSerializable("LIST_ATTACH");
            position = data.getInt("POSITION");
            userName = data.getString("NAME");
            time = data.getString("TIME");

        }

        initData();
    }

    private void initData() {
        adapter = new ViewImageAdapter(this, listData);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        tvUserName.setText(userName);
        tvDate.setText(TimeUtils.getTime(time));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.btn_back)
    public void onClickBack() {
        finish();
    }

    @OnClick(R.id.btn_download)
    public void downloadAttach() {
        DownloadAttachDialog dialog = new DownloadAttachDialog(this);
//        dialog.setUrl(listData.get(position).getDownloadUrl());
        dialog.setNameAttach(listData.get(position).getFileName());
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();

    }
}
