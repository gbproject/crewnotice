package com.crewcloud.apps.crewnotice.dtos;

import com.google.gson.annotations.SerializedName;

public class ErrorDto {

    public boolean unAuthentication;

    @SerializedName("code")
    public int code = 1;

    @SerializedName("message")
    public String message = "";

    @Override
    public String toString() {
        return "ErrorDto{" +
                "unAuthentication=" + unAuthentication +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isUnAuthentication() {
        return unAuthentication;
    }

    public void setUnAuthentication(boolean unAuthentication) {
        this.unAuthentication = unAuthentication;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
