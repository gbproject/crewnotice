package com.crewcloud.apps.crewnotice.activity;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crewcloud.apps.crewnotice.BuildConfig;
import com.crewcloud.apps.crewnotice.Constants;
import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.backup.IntroActivity;
import com.crewcloud.apps.crewnotice.base.WebContentChromeClient;
import com.crewcloud.apps.crewnotice.base.WebContentClient;
import com.crewcloud.apps.crewnotice.dtos.CheckUpdateDto;
import com.crewcloud.apps.crewnotice.dtos.ErrorDto;
import com.crewcloud.apps.crewnotice.interfaces.BaseHTTPCallBack;
import com.crewcloud.apps.crewnotice.loginv2.BaseActivity;
import com.crewcloud.apps.crewnotice.loginv2.LoginV2Activity;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.util.DeviceUtilities;
import com.crewcloud.apps.crewnotice.util.HttpRequest;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.crewcloud.apps.crewnotice.util.Util;
import com.crewcloud.apps.crewnotice.util.WebClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.crewcloud.apps.crewnotice.util.Util.compareVersionNames;

public class MainActivity extends AppCompatActivity {
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 1;
    private WebView wvContent = null;
    private ProgressBar mProgressBar;
    private long noticeNo = 0;
    private final ActivityHandler2 mActivityHandler2 = new ActivityHandler2(this);
    public static String urlDownload = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ShortcutBadger.removeCount(MainActivity.this);
        CrewCloudApplication.getInstance().getPreferenceUtilities().setCountBadge(0);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.hide();
        }

        wvContent = (WebView) findViewById(R.id.wvContent);
        mProgressBar = (ProgressBar) findViewById(R.id.pbProgress);

        Bundle bundleq = getIntent().getExtras();
        if (bundleq != null) {
            try {
                noticeNo = bundleq.getLong(Statics.BUNDLE_NOTICE_NO);
            } catch (Exception e) {
                noticeNo = 0;
            }

        }
        initWebContent(noticeNo);
        checkVersion();
    }

    private void checkVersion() {
        HttpRequest.getInstance().checkVersionUpdate(new BaseHTTPCallBack() {
            @Override
            public void onHTTPSuccess() {

            }

            @Override
            public void onHTTPFail(ErrorDto errorDto) {
                Toast.makeText(getApplicationContext(), errorDto.message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onHTTPSuccess(String response) {
                Gson gson = new Gson();
                CheckUpdateDto checkUpdateDto = gson.fromJson(response, CheckUpdateDto.class);
                //Toast.makeText(getApplicationContext(), checkUpdateDto.getVersion(), Toast.LENGTH_SHORT).show();
                urlDownload = checkUpdateDto.getPackageUrl();
                if (Util.isNetworkAvailable()) {
                    Thread thread = new Thread(new UpdateRunnable(checkUpdateDto.getVersion()));
                    thread.setDaemon(true);
                    thread.start();
                } else {
 //            firstChecking();
                    //Toast.makeText(getApplicationContext(), "Can't connect to server", Toast.LENGTH_SHORT);
                }
            }
        }, this);
    }

    //-----------------------------------start update version 26/3/18
    private class UpdateRunnable implements Runnable {
        String version = "";

        public UpdateRunnable(String version) {
            this.version = version;
        }

        @Override
        public void run() {
            try {
//              URL txtUrl = new URL(Constants.ROOT_URL_ANDROID + Constants.VERSION + Constants.APP_NAME + "_new.txt");
                //   URL txtUrl = new URL(Constants.ROOT_URL_ANDROID + Constants.VERSION + Constants.APP_NAME + ".txt");
                //   HttpURLConnection urlConnection = (HttpURLConnection) txtUrl.openConnection();

               /* InputStream inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String serverVersion = bufferedReader.readLine();
                inputStream.close();
                Util.printLogs("serverVersion: " + serverVersion);*/
                String appVersion = BuildConfig.VERSION_NAME;

               if (compareVersionNames(appVersion, version) == -1) {
                    mActivityHandler2.sendEmptyMessage(Constants.ACTIVITY_HANDLER_START_UPDATE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ActivityHandler2 extends Handler {
        private final WeakReference<MainActivity> mWeakActivity;

        public ActivityHandler2(MainActivity activity) {
            mWeakActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final MainActivity activity = mWeakActivity.get();

            if (activity != null) {
                if (msg.what == Constants.ACTIVITY_HANDLER_NEXT_ACTIVITY) {
                    startApplication();
//                    if (activity.unInstallForOldPackage()) {
//                        return;
//                    }

//                    new WebClientAsyncTask2(activity).execute();
                    //                activity.firstChecking();
                } else if (msg.what == Constants.ACTIVITY_HANDLER_START_UPDATE) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage(R.string.string_update_content);
                    builder.setPositiveButton(R.string.auto_login_button_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new Async_DownloadApkFile(MainActivity.this, Constants.APP_NAME).execute();
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton(R.string.auto_login_button_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            // startApplication();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.setCancelable(false);
                    dialog.show();
                }
            }
        }
    }

    private class Async_DownloadApkFile extends AsyncTask<Void, Void, Void> {
        private String mApkFileName;
        private final WeakReference<MainActivity> mWeakActivity;
        private ProgressDialog mProgressDialog = null;

        public Async_DownloadApkFile(MainActivity activity, String apkFileName) {
            mWeakActivity = new WeakReference<>(activity);
            mApkFileName = apkFileName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            MainActivity activity = mWeakActivity.get();

            if (activity != null) {
                mProgressDialog = new ProgressDialog(activity);
                mProgressDialog.setMessage(getString(R.string.mailActivity_message_download_apk));
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection urlConnection = null;
            InputStream inputStream = null;
            BufferedInputStream bufferedInputStream = null;
            FileOutputStream fileOutputStream = null;

            try {
                //  URL apkUrl = new URL(Constants.ROOT_URL_ANDROID + Constants.PACKGE + mApkFileName + "_new.apk");
           /*     URL apkUrl = new URL(Constants.ROOT_URL_ANDROID + Constants.PACKGE + mApkFileName + ".apk");*/
                URL apkUrl = new URL(urlDownload);
                urlConnection = (HttpURLConnection) apkUrl.openConnection();
                inputStream = urlConnection.getInputStream();
                bufferedInputStream = new BufferedInputStream(inputStream);

                //  String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/download/" + mApkFileName + "_new.apk";
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/download/" + mApkFileName + ".apk";
                fileOutputStream = new FileOutputStream(filePath);

                byte[] buffer = new byte[4096];
                int readCount;

                while (true) {
                    readCount = bufferedInputStream.read(buffer);
                    if (readCount == -1) {
                        break;
                    }

                    fileOutputStream.write(buffer, 0, readCount);
                    fileOutputStream.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (urlConnection != null) {
                    try {
                        urlConnection.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            MainActivity activity = mWeakActivity.get();

            if (activity != null) {
                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/download/" + mApkFileName + ".apk";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (!checkPermissions()) {
                        setPermissions();
                    } else {
                        Uri apkUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", new File(filePath));
                        Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                        intent.setData(apkUri);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        activity.startActivity(intent);
                    }
                } else {
                /*    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(filePath)), "application/vnd.android.package-archive");
                    activity.startActivity(intent);*/
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(filePath)), "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }

            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        }
    }

    private boolean checkPermissions() {
        // android.permission.INTERNET
        // android.permission.WRITE_EXTERNAL_STORAGE

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        return true;
    }

    private void setPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_WIFI_STATE
        }, MY_PERMISSIONS_REQUEST_CODE);
    }

    private final int MY_PERMISSIONS_REQUEST_CODE = 1;

    private void startApplication() {
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

        if (!TextUtils.isEmpty(preferenceUtilities.getCurrentMobileSessionId())) {
            new WebClientAsync_HasApplication_v2().execute();
        } else {
            preferenceUtilities.setCurrentMobileSessionId("");
            preferenceUtilities.setCurrentCompanyNo(0);

            BaseActivity.Instance.callActivity(LoginV2Activity.class);
            finish();
        }
    }

    private class WebClientAsync_HasApplication_v2 extends AsyncTask<Void, Void, Void> {
        private boolean mIsFailed;
        private boolean mHasApplication;
        private String mMessage;

        @Override
        protected Void doInBackground(Void... params) {
            PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

            Util.printLogs("##### Current domain = " + "http://" + preferenceUtilities.getCurrentCompanyDomain());

            WebClient.HasApplication_v2(DeviceUtilities.getLanguageCode(), DeviceUtilities.getTimeZoneOffset(), CrewCloudApplication.getProjectCode(),
                    "http://" + preferenceUtilities.getCurrentCompanyDomain(), new WebClient.OnWebClientListener() {
                        @Override
                        public void onSuccess(JsonNode jsonNode) {
                            try {
                                mIsFailed = false;
                                mHasApplication = jsonNode.get("HasApplication").asBoolean();
                                mMessage = jsonNode.get("Message").asText();
                            } catch (Exception e) {
                                e.printStackTrace();

                                mIsFailed = true;
                                mHasApplication = false;
                                mMessage = getString(R.string.loginActivity_message_wrong_server_site);
                            }
                        }

                        @Override
                        public void onFailure() {
                            mIsFailed = true;
                            mHasApplication = false;
                            mMessage = getString(R.string.loginActivity_message_wrong_server_site);
                        }
                    });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mIsFailed) {
                Toast.makeText(getApplicationContext(), mMessage, Toast.LENGTH_LONG).show();
                finish();
            } else {
                if (mHasApplication) {
                    new WebClientAsync_CheckSessionUser_v2().execute();
                } else {
                    Toast.makeText(getApplicationContext(), mMessage, Toast.LENGTH_LONG).show();
                    new WebClientAsync_Logout_v2().execute();
                }
            }
        }
    }

    private class WebClientAsync_Logout_v2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

            WebClient.Logout_v2(preferenceUtilities.getCurrentMobileSessionId(),
                    "http://" + preferenceUtilities.getCurrentCompanyDomain(), new WebClient.OnWebClientListener() {
                        @Override
                        public void onSuccess(JsonNode jsonNode) {
                        }

                        @Override
                        public void onFailure() {
                        }
                    });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
            preferenceUtilities.setCurrentMobileSessionId("");
            preferenceUtilities.setCurrentCompanyNo(0);
            preferenceUtilities.setCurrentServiceDomain("");
            preferenceUtilities.setCurrentCompanyDomain("");
            preferenceUtilities.setCurrentUserID("");

            finish();
        }
    }

    private class WebClientAsync_CheckSessionUser_v2 extends AsyncTask<Void, Void, Void> {
        private boolean mIsFailed;
        private boolean mIsSuccess;

        @Override
        protected Void doInBackground(Void... params) {
            PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

            WebClient.CheckSessionUser_v2(DeviceUtilities.getLanguageCode(),
                    DeviceUtilities.getTimeZoneOffset(), preferenceUtilities.getCurrentMobileSessionId(),
                    "http://" + preferenceUtilities.getCurrentCompanyDomain(),
                    new WebClient.OnWebClientListener() {
                        @Override
                        public void onSuccess(JsonNode jsonNode) {
                            mIsFailed = false;

                            try {
                                mIsSuccess = (jsonNode.get("success").asInt() == 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                                mIsSuccess = false;
                            }
                        }

                        @Override
                        public void onFailure() {
                            mIsFailed = true;
                            mIsSuccess = false;
                        }
                    });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mIsSuccess) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

                preferenceUtilities.setCurrentMobileSessionId("");
                preferenceUtilities.setCurrentCompanyNo(0);

                Intent intent = new Intent(getApplicationContext(), LoginV2Activity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    ///------------>end update version
    String name, linkDownload;
    public String content;

    private void initWebContent(long noticeNo) {
        WebSettings webSettings = wvContent.getSettings();

        webSettings.setAppCacheEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
        webSettings.setSaveFormData(false);
        webSettings.setSupportZoom(false);
        webSettings.setDomStorageEnabled(true);
        webSettings.setGeolocationEnabled(true);

        wvContent.setWebChromeClient(new WebContentChromeClient());
        wvContent.setWebViewClient(new WebContentClient(this, mProgressBar));

        wvContent.setVerticalScrollBarEnabled(true);
        wvContent.setHorizontalScrollBarEnabled(true);

        wvContent.addJavascriptInterface(new JavaScriptExtension(), "crewcloud");
        wvContent.setDownloadListener(mDownloadListener);
//        wvContent.addJavascriptInterface(new OpenSetting(),"setting");

        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

        String domain = preferenceUtilities.getCurrentCompanyDomain();

        CookieManager.getInstance().setCookie("http://" + domain, "skey0=" + preferenceUtilities.getCurrentMobileSessionId());
        CookieManager.getInstance().setCookie("http://" + domain, "skey1=" + "123123123123132");
        CookieManager.getInstance().setCookie("http://" + domain, "skey2=" + DeviceUtilities.getLanguageCode());
        CookieManager.getInstance().setCookie("http://" + domain, "skey3=" + preferenceUtilities.getCurrentCompanyNo());

        if (noticeNo != 0) {
            wvContent.loadUrl("http://" + domain + "/UI/MobileNotice/Start.aspx?id=" + noticeNo);
        } else {

            wvContent.loadUrl("http://" + domain + "/UI/MobileNotice/");
        }
    }

    private final class JavaScriptExtension {
        @JavascriptInterface
        public void openSetting() {
            BaseActivity.Instance.callActivity(SettingActivity.class);
        }
    }

    // ----------------------------------------------------------------------------------------------

    private boolean mIsBackPressed = false;

    private static class ActivityHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        public ActivityHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = mActivity.get();
            if (activity != null) {
                activity.setBackPressed(false);
            }
        }
    }

    private final ActivityHandler mActivityHandler = new ActivityHandler(this);

    public void setBackPressed(boolean isBackPressed) {
        mIsBackPressed = isBackPressed;
    }

    @Override
    public void onBackPressed() {
        if (wvContent.canGoBack()) {
            wvContent.goBack();
        } else {
            if (!mIsBackPressed) {
                Toast.makeText(this, R.string.mainActivity_message_exit, Toast.LENGTH_SHORT).show();
                mIsBackPressed = true;
                mActivityHandler.sendEmptyMessageDelayed(0, 2000);
            } else {
                finish();
            }
        }
    }

    // ----------------------------------------------------------------------------------------------

    private DownloadManager FileDownloadManager = null;
    private DownloadManager.Request FileDownloadRequest = null;
    private Uri UriToDownload = null;
    private long FileDownloadLatestId = -1;
    private String type;
    private final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment\\s*;\\s*filename\\s*=\\s*\"*([^\"]*)\"*");

    private DownloadListener mDownloadListener = new DownloadListener() {
        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
            linkDownload = url;
            content = contentDisposition;
            type = mimeType;
            URLUtil.guessFileName(url, contentDisposition, mimeType);
            //check permission
            if (ContextCompat.checkSelfPermission((MainActivity.this),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale((MainActivity.this),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            EXTERNAL_STORAGE_PERMISSION_CONSTANT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                downloadFile(url, contentDisposition);
            }
        }
    };

    private void downloadFile(String url, String contentDisposition) {
        new GetFileInfo().execute(url);
    }

    private class GetFileInfo extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            URL url;
            String filename = null;
            try {
                url = new URL(urls[0]);
                Log.d("sssDebug", "url" + url);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                conn.setInstanceFollowRedirects(false);


                String depoSplit[] = conn.getURL().getQuery().split("name=");
                filename = depoSplit[1].split("&")[0];

//                filename = depoSplit[1].replace("filename=", "").replace("\"", "").trim();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
            }
            return filename;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String fileName) {
            super.onPostExecute(fileName);
//            String name = parseContentDisposition(fi)
            name = fileName;

//            Uri uriToDownload = Uri.parse(linkDownload);
//            DownloadManager.Request fileDownloadRequest = new DownloadManager.Request(uriToDownload);
//            fileDownloadRequest.setTitle(fileName);
//            fileDownloadRequest.setDescription("커뮤니티 첨부파일 다운로드");
//            fileDownloadRequest.setDestinationInExternalPublicDir("/Download", fileName);
//            fileDownloadRequest.setVisibleInDownloadsUi(false);
//            fileDownloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//            Environment.getExternalStoragePublicDirectory("/Download").mkdir();
//            mFileDownloadManager.enqueue(fileDownloadRequest);

            DownloadManager.Request request = new DownloadManager.Request(
                    Uri.parse(linkDownload));
            request.setMimeType(type);
            String cookies = CookieManager.getInstance().getCookie(linkDownload);
            request.addRequestHeader("cookie", cookies);
//            request.addRequestHeader("User-Agent", content);
            request.setDescription("Downloading file...");
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);

            Toast.makeText(MainActivity.this, "다운로드를 시작합니다.", Toast.LENGTH_SHORT).show();
//            SnackbarManager.show(
//                    Snackbar.with(getApplicationContext()) // context
//                            .text("Item deleted") // text to display
//                            .actionLabel("Open") // action button label
//                            .actionListener(new ActionClickListener() {
//                                @Override
//                                public void onActionClicked(Snackbar snackbar) {
//                                    Toast.makeText(MainActivity.this, "OPEN", Toast.LENGTH_LONG).show();
//                                }
//                            })
//                    , MainActivity.this);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
        int timezone = preferenceUtilities.getTIME_ZONE();
        int Cur = DeviceUtilities.getTimeZoneOffset();
        if (timezone != Cur) {
            preferenceUtilities.setTIME_ZONE(Cur);
            HttpRequest.getInstance().updateTimeZone(preferenceUtilities.getGCMregistrationid());
        }
    }
}