package com.crewcloud.apps.crewnotice.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by david on 1/5/16.
 */
public abstract class BaseViewClass {

    protected View currentView;
    protected BaseActivity activity;
    protected LayoutInflater inflater;

    public BaseViewClass(BaseActivity activity) {
        this.activity = activity;
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    protected BaseViewClass() {
    }


    protected abstract void setupView();

    public void addToView(ViewGroup view) {
        view.addView(currentView);
    }
}
