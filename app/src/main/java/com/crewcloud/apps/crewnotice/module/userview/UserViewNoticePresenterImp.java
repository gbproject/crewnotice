package com.crewcloud.apps.crewnotice.module.userview;

import android.support.annotation.NonNull;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BasePresenter;
import com.crewcloud.apps.crewnotice.base.BaseResponse;
import com.crewcloud.apps.crewnotice.base.ResponseListener;
import com.crewcloud.apps.crewnotice.data.UserViewNotice;
import com.crewcloud.apps.crewnotice.dtos.ErrorDto;
import com.crewcloud.apps.crewnotice.net.APIService;
import com.crewcloud.apps.crewnotice.net.BodyRequest;
import com.crewcloud.apps.crewnotice.response.MenuResponse;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dazone on 1/12/2017.
 */

public class UserViewNoticePresenterImp extends BasePresenter<UserViewNoticePresenter.view> implements UserViewNoticePresenter.presenter {
    BaseActivity activity;

    public UserViewNoticePresenterImp(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void getUserView(int noticeNo) {
        if (isViewAttached()) {
            activity.showProgressDialog();
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            BodyRequest bodyRequest = new BodyRequest(timeZoneOffset, languageCode, sessionId);
            bodyRequest.setNoticeNo(noticeNo);

            APIService.getInstance().getUserViewNotice(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<List<UserViewNotice>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<List<UserViewNotice>>> result) {
                            activity.dismissProgressDialog();
                            getView().onGetUserViewSuccess(result.getData().getList());
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            activity.dismissProgressDialog();
                            getView().onError(messageResponse);
                        }
                    });
        }
    }
}
