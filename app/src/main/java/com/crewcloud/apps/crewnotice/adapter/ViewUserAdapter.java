package com.crewcloud.apps.crewnotice.adapter;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseAdapter;
import com.crewcloud.apps.crewnotice.data.UserViewNotice;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;
import com.crewcloud.apps.crewnotice.view.AutoSizeTextView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Dazone on 1/9/2017.
 */

public class ViewUserAdapter extends BaseAdapter<UserViewNotice, ViewUserAdapter.ViewHolder> {


    public ViewUserAdapter(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_view, parent, false);
        return new ViewUserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_view_user_iv_avatar)
        CircleImageView ivAvatar;

        @Bind(R.id.item_view_user_rl_check)
        RelativeLayout rlCheck;

        @Bind(R.id.item_view_user_tv_name)
        AutoSizeTextView tvName;

        @Bind(R.id.item_view_user_tv_position)
        AutoSizeTextView tvPosition;

        @Bind(R.id.item_view_user_tv_time)
        TextView tvTime;

        @Bind(R.id.ln_content)
        RelativeLayout lnContent;


        boolean isCheck = false;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(int position) {
            final UserViewNotice userViewNotice = getItem(position);
            tvName.setText(userViewNotice.getUserName());
            tvName.setSingleLine(true);
            tvName.setMinTextSize(11f);
            long time = TimeUtils.getTimeFromString(userViewNotice.getReadDate());
            long today = TimeUtils.getTimeForMail(time);

            itemView.setActivated(userViewNotice.ismIsSelected());

            if (today == -2) {
                //today,hh:mm aa
                tvTime.setText(mActivity.getString(R.string.today) + TimeUtils.displayTimeWithoutOffset(userViewNotice.getReadDate(), true));
            } else {
                //YYY-MM-DD hh:mm aa
                tvTime.setText(TimeUtils.displayTimeWithoutOffset(userViewNotice.getReadDate(), false));
            }
            tvPosition.setText(userViewNotice.getPositionName());

            if (!TextUtils.isEmpty(userViewNotice.getPhotoUrl())) {
                Util.showImage(userViewNotice.getPhotoUrl(), ivAvatar);
            } else {
                Picasso.with(mActivity)
                        .load(R.mipmap.no_photo)
                        .error(R.mipmap.no_photo)
                        .placeholder(R.mipmap.photo_placeholder)
                        .into(ivAvatar);
            }

            // set selected state for wrapper
            itemView.setActivated(userViewNotice.ismIsSelected());
            if (userViewNotice.ismIsSelected()) {
                rlCheck.setAlpha(1.0f);
                rlCheck.setRotationY(0);
            } else {
                rlCheck.setAlpha(0.0f);
            }
//            runAnimationState(userViewNotice);
            itemView.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.btn_press_gray));
//
//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    if (!isCheck) {
//                        isCheck = true;
//                        runAnimationState(userViewNotice);
//                        if (onItemSelectListener != null) {
//
//                            onItemSelectListener.onItemSelect(userViewNotice);
//                        }
//
//                    }
//                    return true;
//                }
//            });
//
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (isCheck) {
//                        isCheck = false;
//                        showHideCheckAnimation(false);
//                        userViewNotice.setmIsSelected(false);
//                        if (onItemSelectListener != null) {
//                            onItemSelectListener.onItemSelect(userViewNotice);
//                        }
//                    }
//
//                }
//            });

        }

        public void runAnimationState(UserViewNotice userDto) {
            if (!userDto.ismIsSelected()) {
                showHideCheckAnimation(true);
                userDto.setmIsSelected(true);
            } else {
                showHideCheckAnimation(false);
                userDto.setmIsSelected(false);
            }
            itemView.setActivated(userDto.ismIsSelected());

            AddOrRemoveMail(userDto);

        }

        private void AddOrRemoveMail(UserViewNotice userViewNotice) {
            if (getItems().contains(userViewNotice)) {
                // remove if already add selected list
                getItems().remove(userViewNotice);
            } else {
                getItems().add(userViewNotice);
            }
        }

        public void showHideCheckAnimation(boolean isShowCheck) {
            if (isShowCheck) {
                final AnimatorSet setRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(mActivity,
                        R.animator.flip_right);

                final AnimatorSet setLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(mActivity,
                        R.animator.flip_left_in);
                setRightOut.setTarget(ivAvatar);
                setRightOut.start();
                setLeftIn.setTarget(rlCheck);
                setLeftIn.start();
            } else {
                final AnimatorSet setRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(mActivity,
                        R.animator.flip_right_out);

                final AnimatorSet setLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(mActivity,
                        R.animator.flip_left_in);
                setRightOut.setTarget(rlCheck);
                setRightOut.start();
                setLeftIn.setTarget(ivAvatar);
                setLeftIn.start();
            }
            itemView.setActivated(isShowCheck);
        }


    }


    public interface OnItemSelectListener {
        void onItemSelect(UserViewNotice userViewNotice);
    }

    private OnItemSelectListener onItemSelectListener;

    public void setOnMailItemSelectListener(OnItemSelectListener listener) {
        this.onItemSelectListener = listener;
    }


}
