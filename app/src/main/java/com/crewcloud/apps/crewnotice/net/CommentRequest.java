package com.crewcloud.apps.crewnotice.net;

/**
 * Created by Dazone on 1/10/2017.
 */

public class CommentRequest {

    private String sessionId;

    private int NoticeNo;

    private String Comment;

    public CommentRequest(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setNoticeNo(int noticeNo) {
        NoticeNo = noticeNo;
    }

    public void setComment(String comment) {
        Comment = comment;
    }
}
