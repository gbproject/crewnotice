package com.crewcloud.apps.crewnotice.loginv2;


import com.crewcloud.apps.crewnotice.BuildConfig;

/**
 * Created by Admin on 8/10/2016.
 */
public class Statics {
    public static final boolean ENABLE_DEBUG = BuildConfig.ENABLE_DEBUG;
    public static final String TAG = "CrewBoard";
    public static final boolean WRITE_HTTP_REQUEST = true;
    public static final int REQUEST_TIMEOUT_MS = 15000;

    public static final String IMAGE_DEAULT = "/Images/Avatar.jpg";


    /**
     * PREFS
     */
    public static final String PREFS_KEY_RELOAD_SETTING = "reload_setting";
    public static final String PREFS_KEY_RELOAD_TIMECARD = "reload_timecard";
    public static final String PREFS_KEY_SESSION_ERROR = "session_error";
    public static final String PREFS_KEY_SORT_STAFF_LIST = "PREFS_KEY_SORT_STAFF_LIST";
    public static final String PREFS_KEY_COMPANY_NAME = "PREFS_KEY_COMPANY_NAME";
    public static final String PREFS_KEY_COMPANY_DOMAIN = "PREFS_KEY_COMPANY_DOMAIN";
    public static final String PREFS_KEY_USER_ID = "PREFS_KEY_USER_ID";

    public static final boolean WRITEHTTPREQUEST = true;
    public static final int FILE_PICKER_SELECT = 1;
    public static final String USER_VIEW_NOTICE = "LIST_USER_VIEW";
    public static final String KEY_PREFERENCES_ADJUST_TO_SCREEN_WIDTH = "SCREEN_WIDTH";


    public abstract class MENU {

        public static final int NOTICE = 1;

    }

    public abstract class ATTACH {
        public static final int CAMERA = 0;
        public static final int PICTURE = 1;
        public static final int VIDEOS = 2;
        public static final int AUDIOS = 3;
        public static final int FILE_ATTACH = 4;
    }

    public static final int SEARCH = 1;
    public static final String ID_NOTICE = "id";
    public static final String COUNT_USER_VIEW = "id";
    public static final String COUNT_OF_ARTICLES = "20";
    public static final int COUNT_LIST = 20;

    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_HH_MM_AA = "hh:mm aa";
    public static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd";

    //file type
    public static final String IMAGE_JPG = ".jpg";
    public static final String IMAGE_JPEG = ".jpeg";
    public static final String IMAGE_PNG = ".png";
    public static final String IMAGE_GIF = ".gif";
    public static final String AUDIO_MP3 = ".mp3";
    public static final String AUDIO_WMA = ".wma";
    public static final String AUDIO_AMR = ".amr";
    public static final String VIDEO_MP4 = ".mp4";
    public static final String VIDEO_MOV = ".mov";
    public static final String FILE_PDF = ".pdf";
    public static final String FILE_DOCX = ".docx";
    public static final String FILE_DOC = ".doc";
    public static final String FILE_XLS = ".xls";
    public static final String FILE_XLSX = ".xlsx";
    public static final String FILE_PPTX = ".pptx";
    public static final String FILE_PPT = ".ppt";
    public static final String FILE_ZIP = ".zip";
    public static final String FILE_RAR = ".rar";
    public static final String FILE_APK = ".apk";
    public static final String MIME_TYPE_IMAGE = "image/*";
    public static final String MIME_TYPE_VIDEO = "video/*";
    public static final String MIME_TYPE_APP = "file/*";
    public static final String MIME_TYPE_TEXT = "text/*";
    public static final String MIME_TYPE_ALL = "*/*";
    public static final String MIME_TYPE_AUDIO = "audio/*";

    public static final String pathDownload = "/DownLoad/";

    public static final String  BUNDLE_NOTICE_NO ="notice_no";
}
