package com.crewcloud.apps.crewnotice.base;

import com.crewcloud.apps.crewnotice.event.MenuEvent;

/**
 * Created by mb on 3/26/16
 */
public class BaseEvent {
    private boolean isLock;
    public BaseEvent() {

    }

    public abstract class EventType {
        public static final int MENU = 1;
        public static final int SETTING = 2;
        public static final int NOTICE_DETAIL = 3;
        public static final int VIEW_USER = 4;
        public static final int VIEW_ATTACH = 5;
        public static final int SENT_EMAIL = 6;
        public static final int LOCK= 7;
    }

    private int type;
    private MenuEvent menuEvent;

    public BaseEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public MenuEvent getMenuEvent() {
        return menuEvent;
    }

    public void setMenuEvent(MenuEvent menuEvent) {
        this.menuEvent = menuEvent;
    }

    public boolean isLock() {
        return isLock;
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }
}
