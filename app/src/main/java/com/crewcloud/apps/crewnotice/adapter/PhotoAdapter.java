package com.crewcloud.apps.crewnotice.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseAdapter;
import com.crewcloud.apps.crewnotice.data.Attachments;
import com.crewcloud.apps.crewnotice.util.Util;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tunglam on 12/16/16.
 */

public class PhotoAdapter extends BaseAdapter<Attachments, PhotoAdapter.ViewHolder> {

    private boolean isAdd;

    public PhotoAdapter(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public PhotoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_attaches, parent, false);
        return new PhotoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_attach_remove)
        ImageView ivRemove;

        @Bind(R.id.item_attach_tv_height)
        TextView tvHeight;

        @Bind(R.id.item_attach_tv_name_attach)
        TextView tvName;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClichAttach(getAdapterPosition());
                    }
                }
            });
        }

        public void bind(int position) {
            Attachments photo = getItem(position);
            tvName.setText(photo.getFileName());
            tvHeight.setText("(" + Util.readableFileSize(photo.getFilelength()) + ")");
            String file_type = Util.getFileName(photo.getFileName());

            if (isAdd) {
                ivRemove.setVisibility(View.VISIBLE);
            } else {
                ivRemove.setVisibility(View.GONE);
            }

//            if (photo.getFileName().endsWith(".jpg")) {
//                if (!TextUtils.isEmpty(photo.getDownloadUrl())) {
//                    Util.showImage(photo.getDownloadUrl(), ivImage);
//                } else {
//                    Picasso.with(getContext()).load(R.mipmap.no_photo)
//                            .placeholder(R.mipmap.photo_placeholder)
//                            .error(R.mipmap.no_photo)
//                            .into(ivImage);
//                }
//            } else {
//                Util.imageFileType(mActivity, ivImage, file_type);
//            }

        }
    }

    public interface ItemClickListener {
        void onItemClichAttach(int position);
    }

    private ItemClickListener listener;

    public void setOnClickItemAttach(ItemClickListener listener) {
        this.listener = listener;
    }
}

