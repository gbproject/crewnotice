package com.crewcloud.apps.crewnotice.di.component;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseFragment;
import com.crewcloud.apps.crewnotice.di.module.AppModule;
import com.crewcloud.apps.crewnotice.di.module.CommonModule;
import com.crewcloud.apps.crewnotice.di.scope.AppScope;

import javax.inject.Named;

import dagger.Component;


/**
 * Created by mb on 7/27/16.
 */
@AppScope
@Component(modules = {AppModule.class, CommonModule.class})
public interface AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);


    Context context();

    @Named("vertical_linear")
    LinearLayoutManager vertical_linear();

    @Named("horizontal_linear")
    LinearLayoutManager horizontal_linear();
}
