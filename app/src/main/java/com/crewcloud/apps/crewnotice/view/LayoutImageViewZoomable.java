package com.crewcloud.apps.crewnotice.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;


/**
 * Created by mb on 5/4/16.
 */
public class LayoutImageViewZoomable extends FrameLayout {
    private ImageViewZoomable imageViewZoomable;

    public LayoutImageViewZoomable(Context context) {
        super(context);
        init();
    }

    public LayoutImageViewZoomable(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LayoutImageViewZoomable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        imageViewZoomable = new ImageViewZoomable(getContext());
        addView(imageViewZoomable, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (Exception e) {

            return false;
        }
    }

    public ImageViewZoomable getImageViewZoomable() {
        return imageViewZoomable;
    }
}
