package com.crewcloud.apps.crewnotice.dialog;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.BaseDialog;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dazone on 1/10/2017.
 */

public class InfoCommentDialog extends BaseDialog {

    private int regUserNo;

    @Bind(R.id.dialog_info_tv_delete)
    TextView tvDelete;

    @Bind(R.id.dialog_info_tv_edit)
    TextView tvEdit;

    public InfoCommentDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_info_comment);
        ButterKnife.bind(this);
    }

    @Override
    public void show() {
        super.show();
        PreferenceUtilities preferenceUtilities = new PreferenceUtilities();
        if (regUserNo != preferenceUtilities.getCurrentUserNo()) {
            tvDelete.setVisibility(View.GONE);
            tvEdit.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.dialog_info_tv_delete)
    public void onClickDelete() {
        if (listener != null) {
            listener.deleteListerner();
        }
    }

    @OnClick(R.id.dialog_info_tv_edit)
    public void onClickEdit() {
        if (listener != null) {
            listener.editListener();
        }
    }

    @OnClick(R.id.dialog_info_tv_copy)
    public void onclickCopy() {
        if (listener!=null){
            listener.copyListener();
        }

    }

    public void setRegUserNo(int regUserNo) {
        this.regUserNo = regUserNo;
    }

    public interface onClickInfoListener {
        void deleteListerner();

        void editListener();
        void copyListener();
    }

    private onClickInfoListener listener;


    public void setOnclickListener(onClickInfoListener listener) {
        this.listener = listener;
    }
}
