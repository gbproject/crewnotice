package com.crewcloud.apps.crewnotice.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dazone on 1/12/2017.
 */

public class UserViewNotice implements Serializable {
    @SerializedName("ReferenceNo")
    private int referenceNo;

    @SerializedName("NoticeNo")
    private int noticeNo;

    @SerializedName("UserNo")
    private int userNo;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("PositionName")
    private String positionName;

    @SerializedName("DepartName")
    private String departName;

    @SerializedName("PhotoUrl")
    private String photoUrl;

    @SerializedName("ReadDate")
    private String readDate;

    @SerializedName("email")
    private String email;

    private boolean mIsSelected;
    private int TypeColor = 2;

    public UserViewNotice(int referenceNo, int noticeNo, int userNo, String userName, String positionName, String departName, String photoUrl, String readDate) {
        this.referenceNo = referenceNo;
        this.noticeNo = noticeNo;
        this.userNo = userNo;
        this.userName = userName;
        this.positionName = positionName;
        this.departName = departName;
        this.photoUrl = photoUrl;
        this.readDate = readDate;
    }

    public UserViewNotice(String name, String email) {
        this.userName = name;
        this.email = email;
    }

    public int getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(int referenceNo) {
        this.referenceNo = referenceNo;
    }

    public int getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(int noticeNo) {
        this.noticeNo = noticeNo;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getReadDate() {
        return readDate;
    }

    public void setReadDate(String readDate) {
        this.readDate = readDate;
    }

    public boolean ismIsSelected() {
        return mIsSelected;
    }

    public void setmIsSelected(boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTypeColor() {
        return TypeColor;
    }

    public void setTypeColor(int typeColor) {
        TypeColor = typeColor;
    }
}


