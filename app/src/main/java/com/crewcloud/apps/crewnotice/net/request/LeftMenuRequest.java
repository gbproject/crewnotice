package com.crewcloud.apps.crewnotice.net.request;

/**
 * Created by dazone on 4/18/2017.
 */

public class LeftMenuRequest {
    private String sessionId;
    private String languageCode;
    private String timeZoneOffset;

    public LeftMenuRequest(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public void setTimeZoneOffset(String timeZoneOffset) {
        this.timeZoneOffset = timeZoneOffset;
    }
}
