package com.crewcloud.apps.crewnotice.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.loginv2.BaseActivity;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.crewcloud.apps.crewnotice.util.Util;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dazone on 5/24/2017.
 */

public class ProfileActivityV2 extends AppCompatActivity {

    @Bind(R.id.activity_new_profile_iv_avatar)
    CircleImageView ivAvatar;

    @Bind(R.id.activity_new_profile_tv_name)
    TextView tvName;
    @Bind(R.id.activity_new_profile_tv_date_of_birth)
    TextView tvBirthDay;
    @Bind(R.id.activity_new_profile_tv_phone)
    TextView tvCellPhone;
    @Bind(R.id.activity_new_profile_tv_date_of_employment)
    TextView tvEntranceDay;
    @Bind(R.id.activity_new_profile_tv_phone_company)
    TextView tvCompanyPhone;
    @Bind(R.id.activity_new_profile_tv_depart_position)
    TextView tvDepartPositionName;

    @Bind(R.id.activity_new_profile_tv_company_name)
    TextView tvCompanyName;

    @Bind(R.id.activity_new_profile_tv_company_id)
    TextView tvCompanyId;

    @Bind(R.id.activity_new_profile_tv_persion_id)
    TextView tvPersionId;

    @Bind(R.id.activity_new_profile_tv_email)
    TextView tvEmail;

    @Bind(R.id.activity_new_profile_tv_pass)
    TextView tvPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_profile);
        ButterKnife.bind(this);

        showData();
    }

    private void showData() {
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
        String companyName = preferenceUtilities.getCompanyName();
        String name = preferenceUtilities.getFullName();
        String email = preferenceUtilities.getEmail();
        String pass = preferenceUtilities.getPass();
        String avatar = preferenceUtilities.getUserAvatar();
        String persionId = preferenceUtilities.getUserID();

        tvName.setText(name);
        tvEmail.setText(email);
        tvPass.setText(pass);
        tvCompanyName.setText(companyName);
        tvPersionId.setText(persionId);
        tvCompanyId.setText(preferenceUtilities.getCurrentCompanyDomain());
        if (!TextUtils.isEmpty(avatar)) {
            Picasso.with(this).load(CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + avatar)
                    .placeholder(R.mipmap.photo_placeholder).into(ivAvatar);
        }
        tvCellPhone.setText(preferenceUtilities.getCellPhone());
        tvCompanyPhone.setText(preferenceUtilities.getCompanyPhone());
        if (!TextUtils.isEmpty(preferenceUtilities.getEntranceDate())) {
            tvEntranceDay.setText(Util.displayTimeWithoutOffset(preferenceUtilities.getEntranceDate()));
        }
        if (!TextUtils.isEmpty(preferenceUtilities.getBirthday())) {
            tvBirthDay.setText(Util.displayTimeWithoutOffset(preferenceUtilities.getBirthday()));
        }

    }

    @Override

    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.btn_back)
    public void onClickBack() {
        finish();
    }

    @OnClick(R.id.change_pass)
    public void changePass() {
        BaseActivity.Instance.callActivity(ChangePasswordActivity.class);
    }

}
