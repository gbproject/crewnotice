package com.crewcloud.apps.crewnotice.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dazone on 4/18/2017.
 */

public class Response<H> {
    @SerializedName("d")
    private H d;

    public H getDataList() {
        return d;
    }

    public void setDataList(H d) {
        this.d = d;
    }
}
