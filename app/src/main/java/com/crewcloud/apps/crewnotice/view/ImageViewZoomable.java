/**
 * Copyright (C) 2013 The Android Open Source Project
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.crewcloud.apps.crewnotice.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 */
public class ImageViewZoomable extends ImageView {

    private String TAG = ImageViewZoomable.class.getSimpleName();

    private PhotoViewAttacher mAttacher;

    public ImageViewZoomable(Context context) {
        super(context, null);
    }

    public ImageViewZoomable(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public ImageViewZoomable(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (mAttacher != null) {
            mAttacher.update();
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (mAttacher != null) {
            mAttacher.update();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mAttacher.cleanup();
        mAttacher = null;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mAttacher = new PhotoViewAttacher(this);
    }


    public void zoomIn(float zoom) {
        if (mAttacher.canZoom()) {
            mAttacher.setScale(mAttacher.getScale() + zoom);
        }
    }


    public void zoomOut(float zoom) {
        if (mAttacher.canZoom()) {
            mAttacher.setScale(mAttacher.getScale() - zoom);
        }
    }
}
