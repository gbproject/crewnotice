package com.crewcloud.apps.crewnotice.module.infocomment;

import android.content.Context;
import android.support.annotation.NonNull;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.base.BasePresenter;
import com.crewcloud.apps.crewnotice.base.BaseResponse;
import com.crewcloud.apps.crewnotice.base.ResponseListener;
import com.crewcloud.apps.crewnotice.dtos.ErrorDto;
import com.crewcloud.apps.crewnotice.net.APIService;
import com.crewcloud.apps.crewnotice.net.BodyRequest;
import com.crewcloud.apps.crewnotice.net.request.EditCommentRequest;
import com.crewcloud.apps.crewnotice.response.MenuResponse;
import com.crewcloud.apps.crewnotice.response.Response;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dazone on 1/10/2017.
 */

public class InfoCommentPresenterImp extends BasePresenter<InfoCommentPresenter.view> implements InfoCommentPresenter.presenter {
    private Context context;

    public InfoCommentPresenterImp(Context context) {
        this.context = context;
    }

    @Override
    public void editComment(String comment, int commentNo,int EditCommentRequest) {
        if (isViewAttached()) {
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            EditCommentRequest bodyRequest = new EditCommentRequest( sessionId);
            bodyRequest.setCommentNo(commentNo);
            bodyRequest.setNoticeNo(EditCommentRequest);
            bodyRequest.setComment(comment);

            APIService.getInstance().editComment(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<Boolean>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<Boolean>>> result) {
                            getView().onEditSuccess("Success");
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            getView().onError(messageResponse);
                        }
                    });
        }
    }

    @Override
    public void deleteComment(int commentNo) {
        if (isViewAttached()) {
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            EditCommentRequest bodyRequest = new EditCommentRequest(sessionId);
            bodyRequest.setCommentNo(commentNo);

            APIService.getInstance().deleteComment(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<Boolean>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<Boolean>>> result) {
                            getView().onEditSuccess("Success");
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            getView().onError(messageResponse);
                        }
                    });
        }
    }
}
