package com.crewcloud.apps.crewnotice.module.notice;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BasePresenter;
import com.crewcloud.apps.crewnotice.base.BaseResponse;
import com.crewcloud.apps.crewnotice.base.ResponseListener;
import com.crewcloud.apps.crewnotice.dtos.ErrorDto;
import com.crewcloud.apps.crewnotice.dtos.Notice;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.net.APIService;
import com.crewcloud.apps.crewnotice.net.request.NoticeRequest;
import com.crewcloud.apps.crewnotice.response.MenuResponse;
import com.crewcloud.apps.crewnotice.response.Response;
import com.crewcloud.apps.crewnotice.util.Util;
import com.crewcloud.apps.crewnotice.util.WebServiceManager;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tunglam on 12/24/16.
 */

public class NoticePresenterImp extends BasePresenter<NoticePresenter.view> implements NoticePresenter.presenter {

    private BaseActivity activity;

    public NoticePresenterImp(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void getNotice(String search, final int divisionNo, final boolean isImportant, int anchorNoticeNo) {
        if (isViewAttached()) {
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage().toUpperCase();
            String timeZoneOffset = "420";
            NoticeRequest bodyRequest = new NoticeRequest(sessionId, languageCode, timeZoneOffset);
            bodyRequest.setDivisionNo(divisionNo);
            bodyRequest.setImportantOnly(isImportant);
            bodyRequest.setSearchText(search);
            bodyRequest.setCountOfArticles(Statics.COUNT_LIST);
            bodyRequest.setAnchorNoticeNo(anchorNoticeNo);

            APIService.getInstance()
                    .getListNotice(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<List<Notice>>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<List<Notice>>>> result) {
                            getView().onGetNoticeSuccess(result.getData().getList().getDataList(), isFromCache());

                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            getView().onError(messageResponse);
                        }

                        @Override
                        public BaseResponse<MenuResponse<Response<List<Notice>>>> onFetchDataFromCacheBG(Realm realm) {
                            BaseResponse<MenuResponse<Response<List<Notice>>>> baseResponse = new BaseResponse<>();
                            MenuResponse<Response<List<Notice>>> listMenuResponse = new MenuResponse<>();
                            Response<List<Notice>> response = new Response<>();
                            List<Notice> lstNotice;
                            if (divisionNo != 0) {
                                lstNotice = realm.where(Notice.class).equalTo("divisionNo", divisionNo).findAll();
                            } else {
                                if (isImportant) {
                                    lstNotice = realm.where(Notice.class).equalTo("important", true).findAll();
                                } else {
                                    lstNotice = realm.where(Notice.class).findAll();
                                }
                            }
                            lstNotice = realm.copyFromRealm(lstNotice);
                            response.setDataList(lstNotice);
                            listMenuResponse.setList(response);
                            baseResponse.setData(listMenuResponse);


                            return baseResponse;
                        }
                    });

//            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
////        String url = "http://192.168.1.22:44316/UI/MobileNotice/NoticeMobileService.asmx/GetNotices";
//            String url = "http://192.168.123.32:44316/UI/MobileNotice/NoticeMobileService.asmx/GetNotices";
//            Map<String, Object> params = new HashMap<>();
//            params.put("languageCode", "EN");
//            params.put("importantOnly", false);
//            params.put("anchorNoticeNo", 1);
//            params.put("sessionId", sessionId);
//            params.put("divisionNo", 0);
//            params.put("countOfArticles", 20);
//            params.put("searchText", "");
//            params.put("timeZoneOffset", "420");
//            WebServiceManager webServiceManager = new WebServiceManager();
//            webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
//                @Override
//                public void onSuccess(String response) {
//                    Log.d("response", response);
//                }
//
//                @Override
//                public void onFailure(ErrorDto error) {
//                    Log.e("ERROR", "onFailure");
//                }
//            });

        }
    }

}
