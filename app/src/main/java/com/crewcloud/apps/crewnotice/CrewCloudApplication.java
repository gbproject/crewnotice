package com.crewcloud.apps.crewnotice;

import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.crewcloud.apps.crewnotice.di.component.AppComponent;
import com.crewcloud.apps.crewnotice.di.component.DaggerAppComponent;
import com.crewcloud.apps.crewnotice.di.module.AppModule;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.crewcloud.apps.crewnotice.view.FontSizes;
import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.fabric.sdk.android.Fabric;

public class CrewCloudApplication extends MultiDexApplication {
    private static CrewCloudApplication mInstance;
    private static PreferenceUtilities mPreferenceUtilities;
    private RequestQueue mRequestQueue;
    private String TAG = "Application";
    private AppComponent appComponent;
    private static final FontSizes fontSizes = new FontSizes();
//    /* authority */
//    private static final String AUTHORITY = BuildConfig.APPLICATION_ID+".provider";
//    /* path */
//    private static final String GET_USER_PATH = "request_user";
//    public static final Uri GET_USER_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + GET_USER_PATH);
    public static String getProjectCode() {
        return "Notice";
    }

    public CrewCloudApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
        configDBBrowser();
        mInstance = this;
    }

    private void configDBBrowser() {
        RealmInspectorModulesProvider provider = RealmInspectorModulesProvider.builder(this)
                .withMetaTables()
                .withDescendingOrder()
                .build();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(provider)
                        .build());
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static synchronized CrewCloudApplication getInstance() {
        return mInstance;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setRetryPolicy(new DefaultRetryPolicy(Statics.REQUEST_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(Statics.REQUEST_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public synchronized PreferenceUtilities getPreferenceUtilities() {
        if (mPreferenceUtilities == null) {
            mPreferenceUtilities = new PreferenceUtilities();
        }

        return mPreferenceUtilities;
    }

    public static FontSizes getFontSizes() {
        return fontSizes;
    }
}