package com.crewcloud.apps.crewnotice.module.leftmenu;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.base.BasePresenter;
import com.crewcloud.apps.crewnotice.base.BaseResponse;
import com.crewcloud.apps.crewnotice.base.ResponseListener;
import com.crewcloud.apps.crewnotice.data.LeftMenu;
import com.crewcloud.apps.crewnotice.net.APIService;
import com.crewcloud.apps.crewnotice.net.request.LeftMenuRequest;
import com.crewcloud.apps.crewnotice.response.MenuResponse;
import com.crewcloud.apps.crewnotice.response.Response;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;

import java.util.List;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tunglam on 12/23/16.
 */

public class LeftMenuPresenterImp extends BasePresenter<LeftMenuPresenter.view> implements LeftMenuPresenter.presenter {

    private Activity activity;

    public LeftMenuPresenterImp(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void getLeftMenu() {
        if (isViewAttached()) {
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage().toUpperCase();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            LeftMenuRequest bodyRequest = new LeftMenuRequest(sessionId);
            bodyRequest.setLanguageCode(languageCode);
            bodyRequest.setTimeZoneOffset(timeZoneOffset);

            APIService.getInstance()
                    .getLeftMenu(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<List<LeftMenu>>>>>() {

                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<List<LeftMenu>>>> result) {
                            Log.d("SUCCESS", result.toString());
                            getView().onGetMenuSuccess(result.getData().getList().getDataList(), isFromCache());
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            getView().onError(messageResponse);
                        }

                        @Override
                        public BaseResponse<MenuResponse<Response<List<LeftMenu>>>> onFetchDataFromCacheBG(Realm realm) {
                            BaseResponse<MenuResponse<Response<List<LeftMenu>>>> baseResponse = new BaseResponse<>();

                            MenuResponse<Response<List<LeftMenu>>> menuResponse = new MenuResponse<>();
                            Response<List<LeftMenu>> response = new Response<>();
                            List<LeftMenu> menuList = realm.where(LeftMenu.class).findAll();
                            menuList = realm.copyFromRealm(menuList);
                            if (menuList != null) {
                                response.setDataList(menuList);
                                menuResponse.setList(response);
                                baseResponse.setData(menuResponse);
                            }
                            return baseResponse;
                        }
                    });
        }

//        if (isViewAttached()) {
//            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
//            String url = "http://192.168.123.32:44316/UI/MobileNotice/NoticeMobileService.asmx/GetDivisions";
//            Map<String, Object> params = new HashMap<>();
//            params.put("sessionId", "" + sessionId);
//            params.put("languageCode", "EN");
//            params.put("timeZoneOffset", "420");
//
//            params.put("isAdmin", "false");
//            params.put("viewCnt", 20);
//            params.put("curPage", 1);
//            params.put("isEndView", "false");
//            params.put("serchType", 0);
//            params.put("serchText", "");
//            params.put("sDate", "01-01-2000");
//            params.put("eDate", "04-18-2027");
//            params.put("sortColumn", "");
//            params.put("divisionNo", 0);
//            params.put("importantOnly", false);

//            WebServiceManager webServiceManager = new WebServiceManager();
//            webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
//                @Override
//                public void onSuccess(String response) {
//                    Log.d("response", response);
//                }
//
//                @Override
//                public void onFailure(ErrorDto error) {
//                    Log.e(TAG, "onFailure");
//                }
//            });
//        }

    }
}
