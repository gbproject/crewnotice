package com.crewcloud.apps.crewnotice.adapter;

import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomButtonsController;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.activity.MainActivity;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseDialog;
import com.crewcloud.apps.crewnotice.base.BaseEvent;
import com.crewcloud.apps.crewnotice.data.Attachments;
import com.crewcloud.apps.crewnotice.data.Comment;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;
import com.crewcloud.apps.crewnotice.dialog.EditCommentDialog;
import com.crewcloud.apps.crewnotice.dialog.InfoCommentDialog;
import com.crewcloud.apps.crewnotice.event.MenuEvent;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;
import com.crewcloud.apps.crewnotice.view.CustomLinearLayoutManager;
import com.crewcloud.apps.crewnotice.view.MessageWebView;
import com.crewcloud.apps.crewnotice.view.MyRecyclerView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;
import butterknife.Bind;
import butterknife.ButterKnife;

import static com.crewcloud.apps.crewnotice.loginv2.Statics.TAG;
import static java.security.AccessController.getContext;

/**
 * Created by tunglam on 12/26/16.
 */

public class NoticeDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private NoticeDetail noticeDetail;
    private BaseActivity mActivity;

    public NoticeDetailAdapter(BaseActivity mActivity) {
        this.mActivity = mActivity;
    }

    public void addAllDetail(NoticeDetail noticeDetails) {
        int curr = getItemCount();
        this.noticeDetail = noticeDetails;
        notifyDataSetChanged();
    }

    public void addAll(List<Comment> comments) {
        int curr = getItemCount();
        noticeDetail.getCommentList().addAll(comments);
        notifyItemRangeInserted(curr, getItemCount());
    }

    public void clear() {
        if (noticeDetail.getCommentList() != null) {
            noticeDetail.getCommentList().clear();
            notifyDataSetChanged();
        }
    }

    public void add(Comment comment) {
        noticeDetail.getCommentList().add(0, comment);
        notifyDataSetChanged();
    }



    public void setNoticeDetail(NoticeDetail noticeDetail) {
        this.noticeDetail = noticeDetail;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (viewType == 0) {
        return new NoticeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notice_detail, parent, false));
//        } else {
//            return new CommentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false));
//        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        if (getItemViewType(position) == 0) {
        ((NoticeViewHolder) holder).bind();
//        } else {
//            ((CommentViewHolder) holder).bind(position);
//        }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class NoticeViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.fragment_notice_detail_lv_attach)
        RecyclerView lvAttach;

        @Bind(R.id.fragment_notice_detail_tv_author)
        TextView tvAuthor;

        @Bind(R.id.fragment_notice_detail_tv_des)
        WebView tvDes;

        @Bind(R.id.fragment_notice_detail_tv_time)
        TextView tvTime;

        @Bind(R.id.fragment_notice_detail_tv_title)
        TextView tvTitle;

        @Bind(R.id.list_comment)
        RecyclerView listComment;

        CommentAdapter commentAdapter;
        InfoCommentDialog infoCommentDialog;
//        ReplyAdapter adapter;

        PhotoAdapter adapter;

        NoticeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            lvAttach.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
            adapter = new PhotoAdapter(mActivity);

            commentAdapter = new CommentAdapter(mActivity);
            listComment.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
//


        }

        public void bind() {
            showData();
        }

        private void showData() {
//            tvDes.configure();
            if (noticeDetail == null) return;
            if (!TextUtils.isEmpty(noticeDetail.getTitle())) {
                tvTitle.setText(Html.fromHtml(noticeDetail.getTitle()));
            } else {
                tvTitle.setText("");
            }
            tvTime.setText(TimeUtils.getTime(noticeDetail.getStartDate()) + " ~ " + TimeUtils.getTime(noticeDetail.getEndDate()));
            adapter.clear();
            lvAttach.setAdapter(adapter);
            adapter.addAll(noticeDetail.getAttaches());
            tvAuthor.setText(noticeDetail.getUserName() + "( " + noticeDetail.getPositionName() + ",/" + noticeDetail.getDepartName() + " )");
            adapter.setOnClickItemAttach(new PhotoAdapter.ItemClickListener() {
                @Override
                public void onItemClichAttach(int position) {
                    BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.VIEW_ATTACH);
                    Bundle data = new Bundle();
                    List<Attachments> list = adapter.getItems();
                    data.putString("NAME", noticeDetail.getUserName());
                    data.putString("TIME", noticeDetail.getModDate());
                    data.putInt("POSITION", position);
                    data.putSerializable("LIST_ATTACH", (Serializable) list);
                    MenuEvent event = new MenuEvent();
                    event.setBundle(data);
                    baseEvent.setMenuEvent(event);
                    EventBus.getDefault().post(baseEvent);
                }
            });
            initWebContent();
            listComment.setAdapter(commentAdapter);
            commentAdapter.clear();
            commentAdapter.addAll(noticeDetail.getCommentList());

            commentAdapter.setOnClickLitener(new CommentAdapter.onClickItemListener() {
            @Override
            public void onLongClickItem(final Comment comment) {
                listener.onClickCommentListener(comment);

            }

            @Override
            public void onClickItem(Comment comment) {

            }
        });
        }


        private void initWebContent() {
//            if (!TextUtils.isEmpty(noticeDetail.getContent())) {
//
//                String content = noticeDetail.getContent();
//                tvDes.setFitsSystemWindows(true);
//                tvDes.setText(content);
//            } else {
//                tvDes.setVisibility(View.GONE);
//            }
            tvDes.setWebViewClient(new WebViewClient());
            tvDes.getSettings().setJavaScriptEnabled(true);
            tvDes.getSettings().setBuiltInZoomControls(true);
            tvDes.getSettings().setLoadWithOverviewMode(true);
            tvDes.getSettings().setDisplayZoomControls(true);
            tvDes.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_POINTER_UP:
                            Toast.makeText(mActivity, "Down Action", Toast.LENGTH_LONG).show();
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            Toast.makeText(mActivity, "Up Action", Toast.LENGTH_LONG).show();
                            break;
                        case MotionEvent.ACTION_SCROLL:
                            Toast.makeText(mActivity, "Scroll Action", Toast.LENGTH_LONG).show();
                            break;
                    }
                    return false;
                }
            });
            String htmlString = noticeDetail.getContent();
            String summary = "<html><body>" + htmlString + "</body></html>";
            tvDes.loadData(summary, "text/html; charset=utf-8", "utf-8");
        }

    }

    public interface onClickCommentListener {
      void onClickCommentListener(Comment comment);
    }

    private onClickCommentListener listener;

    public void setOnClickCommentLitener(onClickCommentListener litener) {
        this.listener = litener;
    }


//    public class CommentViewHolder extends RecyclerView.ViewHolder {
//        @Bind(R.id.item_comment_civ_avatar)
//        ImageView ivAvatar;
//
//        @Bind(R.id.item_comment_iv_info)
//        ImageView ivInfo;
//
//        @Bind(R.id.item_comment_tv_author)
//        TextView tvAuthor;
//
//        @Bind(R.id.item_comment_tv_time)
//        TextView tvTime;
//
//        @Bind(R.id.item_comment_tv_count_comment)
//        TextView tvCountComment;
//
//        @Bind(R.id.item_comment_tv_reply)
//        TextView tvReply;
//
//        @Bind(R.id.item_comment_tv_content)
//        ExpandableTextView tvContent;
//
//        @Bind(R.id.item_comment_list_reply)
//        MyRecyclerView listReply;
//
//        ReplyAdapter adapter;
//
//        public CommentViewHolder(View itemView) {
//            super(itemView);
//            try {
//                ButterKnife.bind(this, itemView);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            adapter = new ReplyAdapter(mActivity);
//
//            // set animation duration via code, but preferable in your layout files by using the animation_duration attribute
//            tvContent.setAnimationDuration(1000L);
//
//            // set interpolators for both expanding and collapsing animations
//            tvContent.setInterpolator(new OvershootInterpolator());
//
//            // or set them separately
//            tvContent.setExpandInterpolator(new OvershootInterpolator());
//            tvContent.setCollapseInterpolator(new OvershootInterpolator());
//
//
//        }
//
//        public void bind(final int position) {
//            final Comment comment = noticeDetail.getCommentList().get(position - 1);
//            if (TextUtils.isEmpty(comment.getAvatarUrl())) {
//                Picasso.with(mActivity).load(R.mipmap.photo_placeholder)
//                        .placeholder(R.mipmap.photo_placeholder)
//                        .error(R.mipmap.no_photo)
//                        .into(ivAvatar);
//            } else {
//                Util.showImage(comment.getAvatarUrl(), ivAvatar);
//            }
//
//            tvAuthor.setText(comment.getUserName());
//            long time = TimeUtils.getTimeFromString(comment.getRegDate());
//            long timeCreated = TimeUtils.getTimeForMail(time);
//            if (timeCreated == -2) {
//                //today,hh:mm aa
//                tvTime.setText(mActivity.getString(R.string.today) + TimeUtils.displayTimeWithoutOffset(comment.getRegDate(), true));
//            } else {
//                //YYY-MM-DD hh:mm aa
//                tvTime.setText(TimeUtils.displayTimeWithoutOffset(comment.getRegDate(), false));
//            }
//            tvContent.setText(comment.getContent());
//            if (comment.getLstReply() != null && comment.getLstReply().size() > 0) {
////                tvCountComment.setText("View More " + noticeDetail.getCommentList().size() + " Comment");
//                tvCountComment.setText(String.valueOf(comment.getLstReply().size()));
//                listReply.setLayoutManager(new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
//                adapter.addAll(comment.getLstReply());
//                listReply.setAdapter(adapter);
//
//            } else {
//                tvReply.setVisibility(View.GONE);
//                tvCountComment.setVisibility(View.GONE);
//            }
//
//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    if (listener != null) {
//                        listener.onLongClickItem(comment);
//                    }
//                    return false;
//                }
//            });
//
//            if (Util.countLines(comment.getContent()) > 3) {
//                ivInfo.setVisibility(View.VISIBLE);
//            } else {
//                ivInfo.setVisibility(View.GONE);
//            }
//            ivInfo.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (tvContent.isExpanded()) {
//                        tvContent.collapse();
//                        ivInfo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_chevron_up_grey600_24dp));
//                    } else {
//                        ivInfo.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_chevron_down_grey600_24dp));
//                        tvContent.expand();
//                    }
//                }
//            });
//            tvContent.setOnExpandListener(new ExpandableTextView.OnExpandListener() {
//                @Override
//                public void onExpand(ExpandableTextView view) {
//                    Log.d(TAG, "ExpandableTextView expanded");
//                }
//
//                @Override
//                public void onCollapse(ExpandableTextView view) {
//                    Log.d(TAG, "ExpandableTextView collapsed");
//                }
//            });
//
//        }
//    }


}