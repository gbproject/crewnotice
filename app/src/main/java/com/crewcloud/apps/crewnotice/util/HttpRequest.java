package com.crewcloud.apps.crewnotice.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.crewcloud.apps.crewnotice.Constants;
import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.dtos.CheckUpdateDto;
import com.crewcloud.apps.crewnotice.dtos.ErrorDto;
import com.crewcloud.apps.crewnotice.dtos.MessageDto;
import com.crewcloud.apps.crewnotice.dtos.UserDto;
import com.crewcloud.apps.crewnotice.interfaces.BaseHTTPCallBack;
import com.crewcloud.apps.crewnotice.interfaces.BaseHTTPCallBackWithString;
import com.crewcloud.apps.crewnotice.interfaces.OnAutoLoginCallBack;
import com.crewcloud.apps.crewnotice.interfaces.OnHasAppCallBack;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.crewcloud.apps.crewnotice.loginv2.Statics.TAG;
import static com.crewcloud.apps.crewnotice.util.Util.getServerSite;

public class HttpRequest {

    private static HttpRequest mInstance;
    private static String root_link;

    public static HttpRequest getInstance() {
        if (null == mInstance) {
            mInstance = new HttpRequest();
        }
        root_link = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentCompanyDomain();

        return mInstance;
    }

    public void login(final BaseHTTPCallBack baseHTTPCallBack, final String userID, final String password, final String companyDomain, String server_link, final String company_name) {
        final String url = server_link + Urls.URL_GET_LOGIN;
        Map<String, String> params = new HashMap<>();
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("timeZoneOffset", "" + Util.getTimeOffsetInMinute());
        params.put("companyDomain", companyDomain);
        params.put("password", password);
        params.put("userID", userID);
        params.put("mobileOSVersion", "Android " + android.os.Build.VERSION.RELEASE);
        Log.d("login", url);
        Log.d("login", params.toString());
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                Log.d("sssDebugRes",response.toString());
                Gson gson = new Gson();
                UserDto userDto = gson.fromJson(response, UserDto.class);
                userDto.prefs.setCurrentMobileSessionId(userDto.session);
                userDto.prefs.setCurrentUserIsAdmin(userDto.PermissionType);
                userDto.prefs.setCurrentCompanyNo(userDto.CompanyNo);
                userDto.prefs.setCurrentUserNo(userDto.Id);
                userDto.prefs.setCurrentUserID(userDto.userID);
                userDto.prefs.setUserAvatar(userDto.avatar);
                userDto.prefs.setCompanyName(userDto.NameCompany);
                userDto.prefs.setEmail(userDto.MailAddress);
                userDto.prefs.setCellPhone(userDto.CellPhone);
                userDto.prefs.setCompanyPhone(userDto.CompanyPhone);
                userDto.prefs.setEntranceDate(userDto.EntranceDate);
                userDto.prefs.setBirthDay(userDto.BirthDate);
                userDto.prefs.setFullName(userDto.FullName);
                userDto.prefs.setPass(password);
                userDto.prefs.setUserID(userID);
                userDto.prefs.setDomain(companyDomain);
                PreferenceUtilities preferenceUtilities = new PreferenceUtilities();

                preferenceUtilities.putStringValue("COMPANY", company_name);

                baseHTTPCallBack.onHTTPSuccess();
            }

            @Override
            public void onFailure(ErrorDto error) {
                baseHTTPCallBack.onHTTPFail(error);
            }
        });
    }

    public void signUp(final BaseHTTPCallBackWithString baseHTTPCallBack, final String email) {
        final String url = Urls.URL_SIGN_UP;
        Map<String, String> params = new HashMap<>();
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("mailAddress", "" + email);
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                MessageDto messageDto = gson.fromJson(response, MessageDto.class);

                if (baseHTTPCallBack != null && messageDto != null) {
                    String message = messageDto.getMessage();
                    baseHTTPCallBack.onHTTPSuccess(message);
                }
            }

            @Override
            public void onFailure(ErrorDto error) {
                if (baseHTTPCallBack != null) {
                    baseHTTPCallBack.onHTTPFail(error);
                }
            }
        });
    }

    public void checkVersionUpdate(final BaseHTTPCallBack baseHTTPCallBack, final Context context) {
        final String url = Urls.URL_CHECK_UPDATE;
        Map<String, String> params = new HashMap<>();
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
        String server_site = preferenceUtilities.getCompanyId();

        server_site = getServerSite(server_site);

        String company_domain = server_site;
        if (!company_domain.startsWith("http")) {
            server_site = "http://" + server_site;
        }
        String temp_server_site = server_site;
        if (temp_server_site.contains(".bizsw.co.kr")) {
            temp_server_site = "http://www.bizsw.co.kr:8080";
        } else {
            if (temp_server_site.contains("crewcloud")) {
                temp_server_site = "http://www.crewcloud.net";
            }
        }
        //  String mTempDomain = "" + preferenceUtilities.getCurrentServiceDomain();
        params.put("Domain", company_domain);
        params.put("MobileType", "Android");
        params.put("Applications", "" + getApplicationName(context));
        Log.d("sssDebug2018", params.toString());
        Log.d("sssDebug2018url", url);

        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                if (baseHTTPCallBack != null) {
                    baseHTTPCallBack.onHTTPSuccess(response);
                }
            }

            @Override
            public void onFailure(ErrorDto error) {
                if (baseHTTPCallBack != null) {
                    baseHTTPCallBack.onHTTPFail(error);
                }
            }
        });
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public void checkLogin(final BaseHTTPCallBack baseHTTPCallBack) {
        final String url = root_link + Urls.URL_CHECK_SESSION;
        Map<String, String> params = new HashMap<>();
        params.put("sessionId", "" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId());
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("timeZoneOffset", "" + Util.getTimeOffsetInMinute());
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                Log.d("sssDebugRess",response.toString());
                Gson gson = new Gson();
                UserDto userDto = gson.fromJson(response, UserDto.class);
                userDto.prefs.setCurrentMobileSessionId(userDto.session);
                userDto.prefs.setCurrentUserIsAdmin(userDto.PermissionType);
                userDto.prefs.setCurrentCompanyNo(userDto.CompanyNo);
                userDto.prefs.setCurrentUserNo(userDto.Id);
                userDto.prefs.setCurrentUserID(userDto.userID);
                //UserDBHelper.addUser(userDto);
                if (baseHTTPCallBack != null) {
                    baseHTTPCallBack.onHTTPSuccess();
                }
            }

            @Override
            public void onFailure(ErrorDto error) {
                if (baseHTTPCallBack != null) {
                    baseHTTPCallBack.onHTTPFail(error);
                }
            }
        });
    }

    public void checkApplication(final OnHasAppCallBack callBack) {
        final String url = root_link + Urls.URL_HAS_APPLICATION;
        Map<String, String> params = new HashMap<>();
        String projectCode = "WorkingTime";

        params.put("projectCode", projectCode);
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("timeZoneOffset", "" + Util.getTimeOffsetInMinute());
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (callBack != null) {
                        if (json.getBoolean("HasApplication")) {
                            callBack.hasApp();
                        } else {
                            ErrorDto errorDto = new ErrorDto();
                            errorDto.message = json.getString("Message");
                            callBack.noHas(errorDto);
                        }
                    }
                } catch (Exception e) {
                    callBack.noHas(new ErrorDto());
                }
            }

            @Override
            public void onFailure(ErrorDto error) {
                callBack.noHas(error);
            }
        });
    }


    public void logout(final BaseHTTPCallBack baseHTTPCallBack) {
        final String url = root_link + Urls.URL_LOG_OUT;
        Map<String, String> params = new HashMap<>();
        params.put("sessionId", "" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId());
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {

                // Clear Login
                CrewCloudApplication.getInstance().getPreferenceUtilities().setCurrentServiceDomain("");
                CrewCloudApplication.getInstance().getPreferenceUtilities().setCurrentUserID("");

                baseHTTPCallBack.onHTTPSuccess();
            }

            @Override
            public void onFailure(ErrorDto error) {
                baseHTTPCallBack.onHTTPFail(error);
            }
        });
    }

    /**
     * AUTO LOGIN
     */
    public void AutoLogin(String companyDomain, String userID, String server_link, final OnAutoLoginCallBack callBack) {
        final String url = server_link + Urls.URL_AUTO_LOGIN;
        Map<String, String> params = new HashMap<>();
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("timeZoneOffset", "" + Util.getTimeOffsetInMinute());
        params.put("companyDomain", companyDomain);
        params.put("userID", userID);
        params.put("mobileOSVersion", "Android " + android.os.Build.VERSION.RELEASE);
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                Util.printLogs("User info =" + response);
                Log.d("sssDebugRess",response.toString());
                Gson gson = new Gson();
                UserDto userDto = gson.fromJson(response, UserDto.class);
                userDto.prefs.setCurrentMobileSessionId(userDto.session);
                userDto.prefs.setCurrentUserIsAdmin(userDto.PermissionType);
                userDto.prefs.setCurrentCompanyNo(userDto.CompanyNo);
                userDto.prefs.setCurrentUserNo(userDto.Id);
                userDto.prefs.setUserAvatar(userDto.avatar);
                userDto.prefs.setCompanyName(userDto.NameCompany);
                userDto.prefs.setEmail(userDto.MailAddress);
                userDto.prefs.setFullName(userDto.FullName);
                userDto.prefs.setUserID(userDto.userID);
                //UserDBHelper.addUser(userDto);

                callBack.OnAutoLoginSuccess(response);
            }

            @Override
            public void onFailure(ErrorDto error) {
                callBack.OnAutoLoginFail(error);
            }
        });
    }


    //----------------------------------------------- Notification ---------------------------------------------------------------
    public static void insertAndroidDevice(final BaseHTTPCallBack callBack, String regid, String json) {
        final String url = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + Constants.URL_INSERT_ANDROID_DEVICE;

        Map<String, String> params = new HashMap<>();
        params.put("sessionId", "" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId());
        params.put("timeZoneOffset", "" + Util.getTimezoneOffsetInMinutes());
        params.put("deviceID", regid);
        params.put("osVersion", "Android " + android.os.Build.VERSION.RELEASE);
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("notificationOptions", json);
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                callBack.onHTTPSuccess();
//                Log.e(TAG,"response:"+response);
            }

            @Override
            public void onFailure(ErrorDto error) {

//                baseHTTPCallBack.onHTTPFail(error);
            }
        });
    }

    public static void updateAndroidDevice(String regid, String json) {
        final String url = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + Constants.URL_UPDATE_ANDROID_DEVICE;

        Map<String, String> params = new HashMap<>();
        params.put("sessionId", "" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId());
        params.put("timeZoneOffset", "" + Util.getTimezoneOffsetInMinutes());
        params.put("deviceID", regid);
        params.put("languageCode", Util.getPhoneLanguage());
        params.put("notificationOptions", json);
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {

                Log.e("Update API", "Update:" + response);
            }

            @Override
            public void onFailure(ErrorDto error) {

//                baseHTTPCallBack.onHTTPFail(error);
            }
        });
    }

    public static void deleteAndroidDevice(final BaseHTTPCallBack callBack) {
        final String url = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + Constants.URL_DELETE_ANDROID_DEVICE;
        Map<String, String> params = new HashMap<>();
        params.put("sessionId", "" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId());
        params.put("timeZoneOffset", "" + Util.getTimezoneOffsetInMinutes());
        params.put("languageCode", Util.getPhoneLanguage());
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {
                Log.e("Delete", " :" + response);
                callBack.onHTTPSuccess();
//                Log.e(TAG,"response:"+response);
            }

            @Override
            public void onFailure(ErrorDto error) {

//                baseHTTPCallBack.onHTTPFail(error);
            }
        });
    }

    public static void updateTimeZone(String regid) {
        final String url = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + Constants.URL_UPDATE_TIMEZONE_ANDROID_DEVICE;

        Map<String, String> params = new HashMap<>();
        params.put("sessionId", "" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId());
        params.put("timeZoneOffset", "" + Util.getTimezoneOffsetInMinutes());
        params.put("deviceID", regid);
        params.put("languageCode", Util.getPhoneLanguage());
        WebServiceManager webServiceManager = new WebServiceManager();
        webServiceManager.doJsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new WebServiceManager.RequestListener<String>() {
            @Override
            public void onSuccess(String response) {

                Log.e(TAG, "Update TimeZone response:" + response);
            }

            @Override
            public void onFailure(ErrorDto error) {

//                baseHTTPCallBack.onHTTPFail(error);
            }
        });
    }
}