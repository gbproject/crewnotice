package com.crewcloud.apps.crewnotice.base;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.interfaces.RequestPermissionCallback;
import com.crewcloud.apps.crewnotice.listener.OnClickOptionMenu;
import com.tbruyelle.rxpermissions.RxPermissions;

import javax.inject.Inject;

import io.realm.Realm;
import rx.functions.Action1;

/**
 * Created by tunglam on 11/10/16.
 */

public class BaseActivity extends AppCompatActivity {
    private int backPressCount;
    private boolean isLock;
    private Dialog mProgressDialog;

    @Inject
    Realm realm;

    public void setTitle(String title) {

    }

    public void changeFragment(BaseFragment fragment, boolean addBackStack,
                               String name) {

    }

    public void changeFragmentBundle(BaseFragment.FragmentEnums fragment, boolean addBackStack,
                                     Bundle bundle) {

    }

    public Realm getRealm() {
        return realm;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CrewCloudApplication.getInstance().getAppComponent().inject(this);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public void setOptionMenu(String title, OnClickOptionMenu listener) {

    }

    public void setOptionMenu(int icon, OnClickOptionMenu listener) {

    }

    public void setLeftMenu(int res) {

    }

    public void setShowActionbarLogo(boolean isShow) {

    }

    public void resetLeftDrawerStatus() {

    }

    public void setActionFloat(boolean isShow) {

    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
            return;
        }
        backPressCount++;
        if (backPressCount > 1) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.mainActivity_message_exit), Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressCount = 0;
                }
            }, 3000);
        }
    }

    public void showProgressDialog() {
        if (null == mProgressDialog || !mProgressDialog.isShowing()) {
            mProgressDialog = new Dialog(this, R.style.ProgressCircleDialog);
            mProgressDialog.setTitle(getString(R.string.loading_content));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setOnCancelListener(null);
            mProgressDialog.addContentView(new ProgressBar(this), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getRealm().close();
    }
}
