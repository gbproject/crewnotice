package com.crewcloud.apps.crewnotice.event;

/**
 * Created by dazone on 5/19/2017.
 */

public class DownloadAttachEvent {
    private int attachNo;
    private String name;

    public DownloadAttachEvent() {
    }

    public void setAttachNo(int attachNo) {
        this.attachNo = attachNo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttachNo() {
        return attachNo;
    }

    public String getName() {
        return name;
    }
}
