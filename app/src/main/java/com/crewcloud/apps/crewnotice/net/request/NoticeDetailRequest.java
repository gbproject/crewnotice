package com.crewcloud.apps.crewnotice.net.request;

/**
 * Created by dazone on 4/19/2017.
 */

public class NoticeDetailRequest {
    private String sessionId;
    private String timeZoneOffset;
    private String languageCode;
    private int NoticeNo;

    public NoticeDetailRequest(String sessionId, int noticeNo) {
        this.sessionId = sessionId;
        NoticeNo = noticeNo;
    }

    public void setTimeZoneOffset(String timeZoneOffset) {
        this.timeZoneOffset = timeZoneOffset;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
