package com.crewcloud.apps.crewnotice.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.data.Attachments;
import com.crewcloud.apps.crewnotice.util.Util;
import com.crewcloud.apps.crewnotice.view.LayoutImageViewZoomable;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Dazone on 1/11/2017.
 */

public class ViewImageAdapter extends PagerAdapter {
    private List<Attachments> imagesURL;
    private LayoutInflater inflater;
    private Context context;

    public ViewImageAdapter(Context context, List<Attachments> imagesURL) {
        this.context = context;
        this.imagesURL = imagesURL;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.row_view_image, view, false);

        assert imageLayout != null;
        final LayoutImageViewZoomable imageView = (LayoutImageViewZoomable) imageLayout.findViewById(R.id.row_view_image);

        Attachments dto = imagesURL.get(position);
        String url = dto.getDownloadUrl();

        Util.showImage(url, imageView.getImageViewZoomable());

        view.addView(imageLayout, 0);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return imageLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imagesURL.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


}
