package com.crewcloud.apps.crewnotice.module.noticedetail;

import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.OkHttpDownloader;
import com.coolerfall.download.Priority;
import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BasePresenter;
import com.crewcloud.apps.crewnotice.base.BaseResponse;
import com.crewcloud.apps.crewnotice.base.ResponseListener;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;
import com.crewcloud.apps.crewnotice.net.APIService;
import com.crewcloud.apps.crewnotice.net.CommentRequest;
import com.crewcloud.apps.crewnotice.net.request.EditCommentRequest;
import com.crewcloud.apps.crewnotice.net.request.NoticeDetailRequest;
import com.crewcloud.apps.crewnotice.response.MenuResponse;
import com.crewcloud.apps.crewnotice.response.Response;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tunglam on 12/24/16.
 */

public class NoticeDetailPresenterImp extends BasePresenter<NoticeDetailPresenter.view> implements NoticeDetailPresenter.presenter {
    private BaseActivity activity;
    private String name = "";

    public NoticeDetailPresenterImp(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void getNoticeDetail(int noticeNo) {
        if (isViewAttached()) {
            activity.showProgressDialog();
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            NoticeDetailRequest bodyRequest = new NoticeDetailRequest(sessionId, noticeNo);
            bodyRequest.setLanguageCode(languageCode);
            bodyRequest.setTimeZoneOffset(timeZoneOffset);

            APIService.getInstance().getDetailNotice(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<NoticeDetail>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<NoticeDetail>>> result) {
                            activity.dismissProgressDialog();
                            getView().onGetDetailSuccess(result.getData().getList().getDataList(), isFromCache());
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            activity.dismissProgressDialog();
                            getView().onError(messageResponse);
                        }

                        @Override
                        public BaseResponse<MenuResponse<Response<NoticeDetail>>> onFetchDataFromCacheBG(Realm realm) {
                            BaseResponse<MenuResponse<Response<NoticeDetail>>> baseResponse = new BaseResponse<>();
                            MenuResponse<Response<NoticeDetail>> menuResponse = new MenuResponse<>();
                            Response<NoticeDetail> response = new Response<>();
                            NoticeDetail noticeDetail = realm.where(NoticeDetail.class).findFirst();
                            noticeDetail = realm.copyFromRealm(noticeDetail);

                            response.setDataList(noticeDetail);
                            menuResponse.setList(response);
                            baseResponse.setData(menuResponse);

                            return baseResponse;
                        }
                    });
        }
    }

    @Override
    public void sentComment(int noticeNo, int regUserNo, String content) {
        if (isViewAttached()) {
            activity.showProgressDialog();
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            CommentRequest bodyRequest = new CommentRequest(sessionId);
            bodyRequest.setNoticeNo(noticeNo);
//            bodyRequest.setRegUserNo(new PreferenceUtilities().getCurrentUserNo());
            bodyRequest.setComment(content);

            APIService.getInstance().insertComment(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<Boolean>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<Boolean>>> result) {
                            activity.dismissProgressDialog();
                            getView().onCommentSuccess("Success");
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            activity.dismissProgressDialog();
                            getView().onError(messageResponse);
                        }
                    });
        }
    }

    @Override
    public void editComment(String comment, int commentNo, int noticeNo) {
        if (isViewAttached()) {
            activity.showProgressDialog();
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            EditCommentRequest bodyRequest = new EditCommentRequest(sessionId);
            bodyRequest.setCommentNo(commentNo);
            bodyRequest.setNoticeNo(noticeNo);
//            bodyRequest.setRegUserNo(new PreferenceUtilities().getCurrentUserNo());
            bodyRequest.setComment(comment);

            APIService.getInstance().editComment(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<Boolean>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<Boolean>>> result) {
                            activity.dismissProgressDialog();
                            getView().onEditSuccess("Success");
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            activity.dismissProgressDialog();
                            getView().onError(messageResponse);
                        }
                    });
        }
    }

    @Override
    public void deleteComment(int commentNo, int noticeNo) {
        if (isViewAttached()) {
            activity.showProgressDialog();
            String sessionId = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
            String languageCode = Util.getPhoneLanguage();
            String timeZoneOffset = String.valueOf(TimeUtils.getTimezoneOffsetInMinutes());
            EditCommentRequest bodyRequest = new EditCommentRequest(sessionId);
            bodyRequest.setCommentNo(commentNo);
            bodyRequest.setNoticeNo(noticeNo);


            APIService.getInstance().deleteComment(bodyRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<BaseResponse<MenuResponse<Response<Boolean>>>>() {
                        @Override
                        public void onSuccess(BaseResponse<MenuResponse<Response<Boolean>>> result) {
                            activity.dismissProgressDialog();
                            getView().onDeleteSuccess("Success");
                        }

                        @Override
                        public void onError(@NonNull String messageResponse) {
                            activity.dismissProgressDialog();
                            getView().onError(messageResponse);
                        }
                    });
        }
    }

    @Override
    public void downloadAttach(int no, String name) {
        if (isViewAttached()) {
//            activity.showProgressDialog();
            this.name = name;
            String url = CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain()
                    + "/UI/Notice/Handlers/FileDownload.ashx?no=" + no + "&name=" + name;
            DownloadManager manager =
                    new DownloadManager.Builder().context(activity)
                            .downloader(OkHttpDownloader.create())
                            .threadPoolSize(2)
                            .build();
            String destPath = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS + File.separator + name;
            DownloadRequest request = new DownloadRequest.Builder()
                    .url(url)
                    .retryTime(5)
                    .retryInterval(2, TimeUnit.SECONDS)
                    .progressInterval(1, TimeUnit.SECONDS)
                    .priority(Priority.HIGH)
                    .allowedNetworkTypes(DownloadRequest.NETWORK_WIFI)
                    .destinationFilePath(destPath)
                    .downloadCallback(new DownloadCallback() {
                        @Override
                        public void onStart(int downloadId, long totalBytes) {

                        }

                        @Override
                        public void onRetry(int downloadId) {

                        }

                        @Override
                        public void onProgress(int downloadId, long bytesWritten, long totalBytes) {

                        }

                        @Override
                        public void onSuccess(int downloadId, String filePath) {
                            getView().onDownloadfileSuccess(filePath, "success");
                            Toast.makeText(activity, "success", Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onFailure(int downloadId, int statusCode, String errMsg) {
                            Toast.makeText(activity, "error", Toast.LENGTH_LONG).show();
                        }
                    })
                    .build();

            int downloadId = manager.add(request);

//            new DownloadFileFromURL().execute(url);
        }
    }

    private class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + name);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            String imagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + name;
            getView().onDownloadfileSuccess("", "");
        }

    }
}
