package com.crewcloud.apps.crewnotice.ui.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.adapter.ListNoticeAdapter;
import com.crewcloud.apps.crewnotice.base.BaseEvent;
import com.crewcloud.apps.crewnotice.base.BaseFragment;
import com.crewcloud.apps.crewnotice.data.LeftMenu;
import com.crewcloud.apps.crewnotice.dtos.Notice;
import com.crewcloud.apps.crewnotice.event.MenuEvent;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.module.notice.NoticePresenter;
import com.crewcloud.apps.crewnotice.module.notice.NoticePresenterImp;
import com.crewcloud.apps.crewnotice.view.MyRecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by tunglam on 12/15/16.
 */

public class NoticeFragment extends BaseFragment implements NoticePresenter.view {

    @Bind(R.id.fragment_notice_list)
    MyRecyclerView rvList;

    @Bind(R.id.fragment_notice_tv_no_data)
    TextView tvNodata;

    ListNoticeAdapter adapter;
    NoticePresenterImp noticePresenterImp;
    LeftMenu leftMenu;
    private String textSearch = "";
    private int divisionId;
    private boolean isImportant = false;
    private boolean stopLoading;
    private int currentPage;

    List<Notice> lstNotice = new ArrayList<>();

    public static BaseFragment newInstance(Bundle bundle) {
        NoticeFragment noticeFragment = new NoticeFragment();
        noticeFragment.setArguments(bundle);
        return noticeFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setTitle(getString(R.string.notice));

        if (getArguments() != null) {
            leftMenu = getArguments().getParcelable("OBJECT");
            if (leftMenu != null) {
                if (leftMenu.getName().equals(getString(R.string.important))) {
                    isImportant = true;
                } else if (leftMenu.getName().equals(getString(R.string.all))) {
                    divisionId = 0;
                } else {
                    divisionId = leftMenu.getDivisionNo();
                }
            }
        }


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notice, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        noticePresenterImp = new NoticePresenterImp(getBaseActivity());
        noticePresenterImp.attachView(this);
        adapter = new ListNoticeAdapter(getBaseActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(linearLayoutManager);
        rvList.setRefresh(true);
        rvList.setAdapter(adapter);
//        adapter.addAll(DataFactory.getListNotice());
        if (adapter.getItemCount() == 0) {
            currentPage = 1;
            stopLoading = true;
            noticePresenterImp.getNotice(textSearch, divisionId, isImportant, currentPage);
        }


        rvList.setMyRecyclerViewListener(new MyRecyclerView.MyRecyclerViewListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                noticePresenterImp.getNotice(textSearch, divisionId, isImportant, currentPage);
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                currentPage = page;
                if (!stopLoading) {
                    noticePresenterImp.getNotice(textSearch, divisionId, isImportant, currentPage);
                }

            }
        });

        adapter.setOnClickNewsListener(new ListNoticeAdapter.OnClickNewsListener() {
            @Override
            public void onItemClicked(int position) {
                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.NOTICE_DETAIL);
                Bundle data = new Bundle();
                data.putInt(Statics.ID_NOTICE, adapter.getItem(position).getNoticeNo());
                MenuEvent menuEvent = new MenuEvent();
                menuEvent.setBundle(data);
                baseEvent.setMenuEvent(menuEvent);
                EventBus.getDefault().post(baseEvent);
            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        if (null != searchView) {

            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getActivity().getComponentName()));

            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

            try {
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchTextView, R.drawable.cursor);
            } catch (Exception ignored) {
            }

        }


        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.LOCK);
                baseEvent.setLock(false);
                EventBus.getDefault().post(baseEvent);
                textSearch = query;
                noticePresenterImp.getNotice(textSearch, divisionId, isImportant, currentPage);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        };


        if (searchView != null) {

            searchView.setOnQueryTextListener(queryTextListener);
            searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {

                @Override
                public void onViewDetachedFromWindow(View arg0) {
                    BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.LOCK);
                    baseEvent.setLock(false);
                    EventBus.getDefault().post(baseEvent);
                    textSearch = "";
                    getBaseActivity().showProgressDialog();
                    noticePresenterImp.getNotice(textSearch, divisionId, isImportant, currentPage);
                }

                @Override
                public void onViewAttachedToWindow(View arg0) {
                    // search was opened
                    Log.d("open,", " open");
                }
            });
        }

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.search:

                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.LOCK);
                baseEvent.setLock(true);
                EventBus.getDefault().post(baseEvent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        noticePresenterImp.detachView();
    }

    @Override
    public void onGetNoticeSuccess(final List<Notice> list, boolean isFromCache) {
        getBaseActivity().dismissProgressDialog();
        Collections.sort(list, new Comparator<Notice>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(Notice list1, Notice list2) {
                boolean b1 = !list1.isImportant();
                boolean b2 = !list2.isImportant();
                return Boolean.compare(b1, b2);
            }
        });

        if (currentPage == 1) {
            adapter.clear();
            adapter.notifyDataSetChanged();
        }
        stopLoading = list.size() == 0 || isFromCache;
        lstNotice.addAll(list);
        adapter.addAll(list);
        rvList.setNotShowLoadMore(stopLoading);
        rvList.stopShowLoading();

        if (adapter.getItems().size() > 0) {
            tvNodata.setVisibility(View.GONE);
            rvList.setVisibility(View.VISIBLE);
        } else {
            tvNodata.setVisibility(View.VISIBLE);
            rvList.setVisibility(View.GONE);
        }

        if (!isFromCache) {
            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
//                    if (divisionId != 0) {
//                        realm.where(Notice.class).equalTo("divisionNo", divisionId).findAll().deleteAllFromRealm();
//                        realm.insert(list);
//                    } else {
//                        if (isImportant) {
//                            realm.where(Notice.class).equalTo("important", true).findAll().deleteAllFromRealm();
//                            realm.insert(list);
//                        } else {
                    realm.where(Notice.class).findAll().deleteAllFromRealm();
                    realm.insert(list);
//                        }
//                }

                }
            });
        }

    }


    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

    }
}
