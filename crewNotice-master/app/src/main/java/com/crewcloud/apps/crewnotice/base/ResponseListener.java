package com.crewcloud.apps.crewnotice.base;

import com.crewcloud.apps.crewnotice.interfaces.ActionCallback;
import com.crewcloud.apps.crewnotice.util.BackgroundUtils;
import com.crewcloud.apps.crewnotice.util.Util;

import io.fabric.sdk.android.services.network.NetworkUtils;
import io.realm.Realm;
import rx.Observer;

/**
 * Created by mb on 3/18/16
 */
public abstract class ResponseListener<T> implements Observer<T> {
    private boolean fromCache;

    @Override
    public final void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        if (!Util.isNetworkAvailable()) {
            BackgroundUtils.doAction(new ActionCallback<T>() {
                @Override
                public T onBackground() {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    T t = onFetchDataFromCacheBG(realm);
                    realm.commitTransaction();
                    return t;
                }

                @Override
                public void onForeground(T result) {
                    if (result != null) {
                        fromCache = true;
                        onSuccess(result);
                    } else {
                        onError("Error,Please try again.");
                    }
                }
            });
            return;
        }

        onError("Error,Please try again.");
    }

    @Override
    public void onNext(T s) {
        onSuccess(s);
        if (!fromCache) {
            try {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                onSaveData(s, realm);
                realm.commitTransaction();
            } catch (Throwable e) {
            }
        }
    }

    public abstract void onSuccess(T result);

    public abstract void onError(String messageResponse);

    public T onFetchDataFromCacheBG(Realm realm) {
        return null;
    }

    public boolean isFromCache() {
        return fromCache;
    }

    public void onSaveData(T result, Realm realm) {

    }
}
