package com.crewcloud.apps.crewnotice.data;

/**
 * Created by Dazone on 1/11/2017.
 */

public class Selection {

    private String name;

    private int file;

    public Selection(String name, int file) {
        this.name = name;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFile() {
        return file;
    }

    public void setFile(int file) {
        this.file = file;
    }
}
