package com.crewcloud.apps.crewnotice.net;

import com.crewcloud.apps.crewnotice.base.BaseResponse;
import com.crewcloud.apps.crewnotice.data.Comment;
import com.crewcloud.apps.crewnotice.data.LeftMenu;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;
import com.crewcloud.apps.crewnotice.data.UserViewNotice;
import com.crewcloud.apps.crewnotice.dtos.Notice;
import com.crewcloud.apps.crewnotice.net.request.EditCommentRequest;
import com.crewcloud.apps.crewnotice.net.request.LeftMenuRequest;
import com.crewcloud.apps.crewnotice.net.request.NoticeDetailRequest;
import com.crewcloud.apps.crewnotice.net.request.NoticeRequest;
import com.crewcloud.apps.crewnotice.response.MenuResponse;
import com.crewcloud.apps.crewnotice.response.Response;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import io.realm.RealmList;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by mb on 3/30/16.
 */
public interface MyApi {

    @FormUrlEncoded
    @POST("/UI/WebService/WebServiceCenter.asmx/Login_v2")
    Observable<JSONObject> login(@FieldMap Map<String, String> params);

    @POST("/UI/MobileNotice/NoticeMobileService.asmx/GetDivisions")
    Observable<BaseResponse<MenuResponse<Response<List<LeftMenu>>>>> getLeftMenu(@Body LeftMenuRequest param);

    @POST("/UI/MobileNotice/NoticeMobileService.asmx/GetNotices")
    Observable<BaseResponse<MenuResponse<Response<List<Notice>>>>> getListNotice(@Body NoticeRequest param);

    @POST("/UI/MobileNotice/NoticeMobileService.asmx/GetContentNotice")
    Observable<BaseResponse<MenuResponse<Response<NoticeDetail>>>> getDetailNotice(@Body NoticeDetailRequest param);

    @POST("/UI/MobileNotice/NoticeMobileService.asmx/GetComment")
    Observable<BaseResponse<MenuResponse<Response<RealmList<Comment>>>>> getComment(@Body NoticeDetailRequest param);

    @POST("/UI/MobileNotice/NoticeMobileService.asmx/AddComment")
    Observable<BaseResponse<MenuResponse<Response<Boolean>>>> insertComment(@Body CommentRequest param);

        @POST("/UI/MobileNotice/NoticeMobileService.asmx/EditComment")
    Observable<BaseResponse<MenuResponse<Response<Boolean>>>> editComment(@Body EditCommentRequest param);

    @POST("/UI/MobileNotice/NoticeMobileService.asmx/DeleteComment")
    Observable<BaseResponse<MenuResponse<Response<Boolean>>>> deleteComment(@Body EditCommentRequest param);


    @POST("/UI/MobileNotice/NoticeMobileService.asmx/GetListOfReference")
    Observable<BaseResponse<MenuResponse<List<UserViewNotice>>>> getUserViewNotice(@Body BodyRequest param);
}