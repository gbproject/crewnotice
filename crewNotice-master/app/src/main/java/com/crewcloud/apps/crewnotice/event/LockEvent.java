package com.crewcloud.apps.crewnotice.event;

/**
 * Created by Dazone on 1/9/2017.
 */

public class LockEvent {
    private String id;
    private boolean isLock;

    public LockEvent() {
    }

    public boolean isLock() {
        return isLock;
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
