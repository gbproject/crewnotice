package com.crewcloud.apps.crewnotice.module.infocomment;

import com.crewcloud.apps.crewnotice.base.BaseView;

/**
 * Created by Dazone on 1/10/2017.
 */

public interface InfoCommentPresenter {
    interface view extends BaseView {
        void onDeleteSuccess(String message);

        void onEditSuccess(String message);

        void onError(String message);
    }

    interface presenter {
        void editComment(String comment, int commentNo,int noticeNo);

        void deleteComment(int commentNo);
    }
}
