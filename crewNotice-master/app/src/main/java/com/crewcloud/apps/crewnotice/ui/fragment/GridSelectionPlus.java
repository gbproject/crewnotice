package com.crewcloud.apps.crewnotice.ui.fragment;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.adapter.GridAdapterPlus;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseViewClass;
import com.crewcloud.apps.crewnotice.factory.DataFactory;
import com.crewcloud.apps.crewnotice.loginv2.Statics;

import butterknife.ButterKnife;

/**
 * Created by Dazone on 1/11/2017.
 */
public class GridSelectionPlus extends BaseViewClass {

    public static final int TAKE_PICTURE = 1;
    public static final int RESULT_LOAD_IMAGE = 2;
    public static final int AUDIO = 3;
    public static final int VIDEO = 4;

    public GridSelectionPlus(BaseActivity activity) {
        super(activity);
        setupView();
    }

    @Override
    protected void setupView() {
        currentView = inflater.inflate(R.layout.view_plus, null);
        RecyclerView rvList = (RecyclerView) currentView.findViewById(R.id.view_plus_recycle_view);
        ButterKnife.bind(this, currentView);

        GridAdapterPlus adapterPlus = new GridAdapterPlus(activity);
        rvList.setLayoutManager(new GridLayoutManager(activity, 3));
        rvList.setAdapter(adapterPlus);
        adapterPlus.addAll(DataFactory.getMenuSelection(activity));

        adapterPlus.setOnClickListerner(new GridAdapterPlus.onItemListener() {
            @Override
            public void onClickItemListerner(int position) {
                switch (position) {
                    case Statics.ATTACH.CAMERA:
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        activity.startActivityForResult(intent, TAKE_PICTURE);
                        break;
                    case Statics.ATTACH.PICTURE:

                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        activity.startActivityForResult(i, RESULT_LOAD_IMAGE);
                        // open Picture
                        break;
                    case Statics.ATTACH.VIDEOS:
                        //open videos
                        Intent videoIntent = new Intent();
                        videoIntent.setType("videos/*");
                        videoIntent.setAction(Intent.ACTION_GET_CONTENT);
                        activity.startActivityForResult(videoIntent, VIDEO);
                        break;

                    case Statics.ATTACH.AUDIOS:
                        Intent intent_upload = new Intent();
                        intent_upload.setType("audio/*");
                        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                        activity.startActivityForResult(intent_upload, AUDIO);
                        break;
                    case Statics.ATTACH.FILE_ATTACH:
                        break;
                }
            }
        });
    }

}
