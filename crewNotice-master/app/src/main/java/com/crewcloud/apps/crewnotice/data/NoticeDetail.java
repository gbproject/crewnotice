package com.crewcloud.apps.crewnotice.data;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by tunglam on 12/24/16.
 */

public class NoticeDetail extends RealmObject {

    @SerializedName("NoticeNo")
    private int noticeNo;

    @SerializedName("RegUserNo")
    private int regUserNo;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("RegDate")
    private String regDate;

    @SerializedName("PositionName")
    private String positionName;

    @SerializedName("DepartName")
    private String departName;

    @SerializedName("ModUserNo")
    private int modUserNo;

    @SerializedName("ModDate")
    private String modDate;

    @SerializedName("Title")
    private String title;

    @SerializedName("DivisionNo")
    private int divisionNo;

    @SerializedName("Content")
    private String content;
    @SerializedName("DivisionName")
    private String divisionName;

    @SerializedName("StartDate")
    private String startDate;

    @SerializedName("EndDate")
    private String endDate;

    @SerializedName("Important")
    private boolean isImportant;

    @SerializedName("IsShare")
    private boolean isShare;

    @SerializedName("IsAttach")
    private boolean isAttach;

    @SerializedName("TotalViews")
    private int totalView;

    @SerializedName("CurrentViews")
    private int currentViews;

    @SerializedName("IsContentImg")
    private boolean isContentImg;

    @SerializedName("ViewUserCnt")
    private int viewUserCnt;

    @SerializedName("Other")
    private String other;

    @SerializedName("MoveType")
    private String MoveType;

    @SerializedName("Attachments")
    private RealmList<Attachments> attaches;

    @SerializedName("Comments")
    private RealmList<Comment> commentList;

    public NoticeDetail() {
    }

    public int getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(int noticeNo) {
        this.noticeNo = noticeNo;
    }

    public int getRegUserNo() {
        return regUserNo;
    }

    public void setRegUserNo(int regUserNo) {
        this.regUserNo = regUserNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }


    public int getModUserNo() {
        return modUserNo;
    }

    public void setModUserNo(int modUserNo) {
        this.modUserNo = modUserNo;
    }

    public String getModDate() {
        return modDate;
    }

    public void setModDate(String modDate) {
        this.modDate = modDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDivisionNo() {
        return divisionNo;
    }

    public void setDivisionNo(int divisionNo) {
        this.divisionNo = divisionNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }

    public boolean isAttach() {
        return isAttach;
    }

    public void setAttach(boolean attach) {
        isAttach = attach;
    }

    public int getTotalView() {
        return totalView;
    }

    public void setTotalView(int totalView) {
        this.totalView = totalView;
    }

    public int getCurrentViews() {
        return currentViews;
    }

    public void setCurrentViews(int currentViews) {
        this.currentViews = currentViews;
    }

    public boolean isContentImg() {
        return isContentImg;
    }

    public void setContentImg(boolean contentImg) {
        isContentImg = contentImg;
    }

    public RealmList<Attachments> getAttaches() {
        return attaches;
    }

    public void setAttaches(RealmList<Attachments> attaches) {
        this.attaches = attaches;
    }

    public RealmList<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(RealmList<Comment> commentList) {
        this.commentList = commentList;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }

    public int getViewUserCnt() {
        return viewUserCnt;
    }

    public void setViewUserCnt(int viewUserCnt) {
        this.viewUserCnt = viewUserCnt;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getMoveType() {
        return MoveType;
    }

    public void setMoveType(String moveType) {
        MoveType = moveType;
    }
}
