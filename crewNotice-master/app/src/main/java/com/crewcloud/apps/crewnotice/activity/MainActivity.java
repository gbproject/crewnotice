package com.crewcloud.apps.crewnotice.activity;

import android.Manifest;
import android.app.DownloadManager;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.WebContentChromeClient;
import com.crewcloud.apps.crewnotice.base.WebContentClient;
import com.crewcloud.apps.crewnotice.loginv2.BaseActivity;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.util.DeviceUtilities;
import com.crewcloud.apps.crewnotice.util.HttpRequest;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 1;
    private WebView wvContent = null;
    private ProgressBar mProgressBar;
    private long noticeNo=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.hide();
        }

        wvContent = (WebView) findViewById(R.id.wvContent);
        mProgressBar = (ProgressBar) findViewById(R.id.pbProgress);

        Bundle bundleq = getIntent().getExtras();
        if (bundleq != null) {
            try {
                noticeNo = bundleq.getLong(Statics.BUNDLE_NOTICE_NO);
            } catch (Exception e) {
                noticeNo = 0;
            }

        }
        initWebContent(noticeNo);
    }

    String name, linkDownload;
    public String content;

    private void initWebContent(long noticeNo) {
        WebSettings webSettings = wvContent.getSettings();

        webSettings.setAppCacheEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
        webSettings.setSaveFormData(false);
        webSettings.setSupportZoom(false);
        webSettings.setDomStorageEnabled(true);
        webSettings.setGeolocationEnabled(true);

        wvContent.setWebChromeClient(new WebContentChromeClient());
        wvContent.setWebViewClient(new WebContentClient(this, mProgressBar));

        wvContent.setVerticalScrollBarEnabled(true);
        wvContent.setHorizontalScrollBarEnabled(true);

        wvContent.addJavascriptInterface(new JavaScriptExtension(), "crewcloud");
        wvContent.setDownloadListener(mDownloadListener);
//        wvContent.addJavascriptInterface(new OpenSetting(),"setting");

        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();

        String domain = preferenceUtilities.getCurrentCompanyDomain();

        CookieManager.getInstance().setCookie("http://" + domain, "skey0=" + preferenceUtilities.getCurrentMobileSessionId());
        CookieManager.getInstance().setCookie("http://" + domain, "skey1=" + "123123123123132");
        CookieManager.getInstance().setCookie("http://" + domain, "skey2=" + DeviceUtilities.getLanguageCode());
        CookieManager.getInstance().setCookie("http://" + domain, "skey3=" + preferenceUtilities.getCurrentCompanyNo());

        if(noticeNo!=0){
            wvContent.loadUrl("http://" + domain + "/UI/MobileNotice/Start.aspx?id="+noticeNo);
        }else {

            wvContent.loadUrl("http://" + domain + "/UI/MobileNotice/");
        }
    }

    private final class JavaScriptExtension {
        @JavascriptInterface
        public void openSetting() {
            BaseActivity.Instance.callActivity(SettingActivity.class);
        }
    }

    // ----------------------------------------------------------------------------------------------

    private boolean mIsBackPressed = false;

    private static class ActivityHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        public ActivityHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = mActivity.get();
            if (activity != null) {
                activity.setBackPressed(false);
            }
        }
    }

    private final ActivityHandler mActivityHandler = new ActivityHandler(this);

    public void setBackPressed(boolean isBackPressed) {
        mIsBackPressed = isBackPressed;
    }

    @Override
    public void onBackPressed() {
        if (wvContent.canGoBack()) {
            wvContent.goBack();
        } else {
            if (!mIsBackPressed) {
                Toast.makeText(this, R.string.mainActivity_message_exit, Toast.LENGTH_SHORT).show();
                mIsBackPressed = true;
                mActivityHandler.sendEmptyMessageDelayed(0, 2000);
            } else {
                finish();
            }
        }
    }

    // ----------------------------------------------------------------------------------------------

    private DownloadManager FileDownloadManager = null;
    private DownloadManager.Request FileDownloadRequest = null;
    private Uri UriToDownload = null;
    private long FileDownloadLatestId = -1;
    private String type;
    private final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment\\s*;\\s*filename\\s*=\\s*\"*([^\"]*)\"*");

    private DownloadListener mDownloadListener = new DownloadListener() {
        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
            linkDownload = url;
            content = contentDisposition;
            type = mimeType;
            URLUtil.guessFileName(url, contentDisposition, mimeType);
            //check permission
            if (ContextCompat.checkSelfPermission((MainActivity.this),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale((MainActivity.this),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            EXTERNAL_STORAGE_PERMISSION_CONSTANT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                downloadFile(url, contentDisposition);
            }
        }
    };

    private void downloadFile(String url, String contentDisposition) {
        new GetFileInfo().execute(url);
    }

    private class GetFileInfo extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            URL url;
            String filename = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                conn.setInstanceFollowRedirects(false);


                String depoSplit[] = conn.getURL().getQuery().split("name=");
                filename = depoSplit[1].split("&")[0];

//                filename = depoSplit[1].replace("filename=", "").replace("\"", "").trim();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
            }
            return filename;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String fileName) {
            super.onPostExecute(fileName);
//            String name = parseContentDisposition(fi)
            name = fileName;

//            Uri uriToDownload = Uri.parse(linkDownload);
//            DownloadManager.Request fileDownloadRequest = new DownloadManager.Request(uriToDownload);
//            fileDownloadRequest.setTitle(fileName);
//            fileDownloadRequest.setDescription("커뮤니티 첨부파일 다운로드");
//            fileDownloadRequest.setDestinationInExternalPublicDir("/Download", fileName);
//            fileDownloadRequest.setVisibleInDownloadsUi(false);
//            fileDownloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//            Environment.getExternalStoragePublicDirectory("/Download").mkdir();
//            mFileDownloadManager.enqueue(fileDownloadRequest);

            DownloadManager.Request request = new DownloadManager.Request(
                    Uri.parse(linkDownload));
            request.setMimeType(type);
            String cookies = CookieManager.getInstance().getCookie(linkDownload);
            request.addRequestHeader("cookie", cookies);
//            request.addRequestHeader("User-Agent", content);
            request.setDescription("Downloading file...");
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);

            Toast.makeText(MainActivity.this, "다운로드를 시작합니다.", Toast.LENGTH_SHORT).show();
//            SnackbarManager.show(
//                    Snackbar.with(getApplicationContext()) // context
//                            .text("Item deleted") // text to display
//                            .actionLabel("Open") // action button label
//                            .actionListener(new ActionClickListener() {
//                                @Override
//                                public void onActionClicked(Snackbar snackbar) {
//                                    Toast.makeText(MainActivity.this, "OPEN", Toast.LENGTH_LONG).show();
//                                }
//                            })
//                    , MainActivity.this);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
        int timezone = preferenceUtilities.getTIME_ZONE();
        int Cur = DeviceUtilities.getTimeZoneOffset();
        if (timezone != Cur) {
            preferenceUtilities.setTIME_ZONE(Cur);
            HttpRequest.getInstance().updateTimeZone(preferenceUtilities.getGCMregistrationid());
        }
    }
}