package com.crewcloud.apps.crewnotice.net.request;

/**
 * Created by dazone on 5/18/2017.
 */

public class EditCommentRequest {
    private int CommentNo;
    private int NoticeNo;
    private String Comment;
    private String sessionId;

    public EditCommentRequest(String sessionId) {
        this.sessionId = sessionId;
    }

    public EditCommentRequest() {
    }

    public void setCommentNo(int commentNo) {
        CommentNo = commentNo;
    }

    public void setNoticeNo(int noticeNo) {
        NoticeNo = noticeNo;
    }

    public void setComment(String comment) {
        Comment = comment;
    }
}
