package com.crewcloud.apps.crewnotice.module.noticedetail;

import com.crewcloud.apps.crewnotice.base.BaseView;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;

/**
 * Created by tunglam on 12/24/16.
 */

public interface NoticeDetailPresenter {
    interface view extends BaseView {
        void onGetDetailSuccess(NoticeDetail noticeDetail, boolean isFromCached);

        void onCommentSuccess(String message);

        void onError(String message);

        void onDeleteSuccess(String message);

        void onEditSuccess(String message);

        void onDownloadfileSuccess(String path,String newUrl);

    }

    interface presenter {
        void getNoticeDetail(int noticeNo);

        void sentComment(int noticeNo, int regUserNo, String content);

        void editComment(String comment, int commentNo, int noticeNo);

        void deleteComment(int commentNo, int noticeNo);

        void downloadAttach(int no, String name);
    }
}
