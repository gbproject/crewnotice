package com.crewcloud.apps.crewnotice.di.module;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.di.scope.AppScope;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;

/**
 * Created by mb on 7/27/16.
 */
@Module
@AppScope
public class AppModule {

    public AppModule() {
    }

    @Provides
    @AppScope
    protected Application provideApplication() {
        return CrewCloudApplication.getInstance();
    }

    @Provides
    public Realm provideRealm(RealmConfiguration realmConfiguration) {
        Realm.setDefaultConfiguration(realmConfiguration);
        return Realm.getDefaultInstance();
    }

    @Provides
    @AppScope
    protected RealmConfiguration provideRealmConfig(Application application, RealmMigration realmMigration) {
        return new RealmConfiguration
                .Builder(application)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .migration(realmMigration)
                .build();
    }

    @Provides
    @AppScope
    protected RealmMigration provideRealmMigration() {
        return new RealmMigration() {
            @Override
            public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

            }
        };
    }

    @Provides
    Context provideContext() {
        return CrewCloudApplication.getInstance();
    }

}
