package com.crewcloud.apps.crewnotice.ui.fragment;

import android.Manifest;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.adapter.CommentAdapter;
import com.crewcloud.apps.crewnotice.adapter.PhotoAdapter;
import com.crewcloud.apps.crewnotice.base.BaseDialog;
import com.crewcloud.apps.crewnotice.base.BaseFragment;
import com.crewcloud.apps.crewnotice.data.Comment;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;
import com.crewcloud.apps.crewnotice.dialog.DownloadAttachDialog;
import com.crewcloud.apps.crewnotice.dialog.EditCommentDialog;
import com.crewcloud.apps.crewnotice.dialog.InfoCommentDialog;
import com.crewcloud.apps.crewnotice.event.DownloadAttachEvent;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.module.noticedetail.NoticeDetailPresenter;
import com.crewcloud.apps.crewnotice.module.noticedetail.NoticeDetailPresenterImp;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.crewcloud.apps.crewnotice.util.TimeUtils;
import com.crewcloud.apps.crewnotice.util.Util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by jerry on 12/16/16 .
 */

public class NoticeDetailFragment extends BaseFragment implements NoticeDetailPresenter.view {


    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 1;
    @Bind(R.id.fragment_notice_detail_et_comment)
    EditText etComment;

    @Bind(R.id.fragment_notice_detail_iv_send)
    ImageView ivSend;

    @Bind(R.id.fragment_notice_detail_lv_attach)
    RecyclerView lvAttach;

    @Bind(R.id.fragment_notice_detail_tv_author)
    TextView tvAuthor;

    @Bind(R.id.fragment_notice_detail_tv_des)
    WebView tvDes;

    @Bind(R.id.fragment_notice_detail_tv_time)
    TextView tvTime;

    @Bind(R.id.fragment_notice_detail_tv_title)
    TextView tvTitle;

    @Bind(R.id.item_notice_detail_iv_important)
    ImageView ivImportant;

    @Bind(R.id.list_comment)
    RecyclerView listComment;

    CommentAdapter commentAdapter;
    //        ReplyAdapter adapter;
    int no;
    String name;

    PhotoAdapter photoAdapter;


    InfoCommentDialog infoCommentDialog;

    NoticeDetailPresenterImp noticeDetailPresenterImp;
    NoticeDetail noticeDetail;
    PhotoAdapter attachAdapter;
    private int idNotice;

    public static BaseFragment newInstance(Bundle bundle) {
        NoticeDetailFragment fragment = new NoticeDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.detail));
        setActionFloat(true);
        setHasOptionsMenu(false);
        EventBus.getDefault().register(this);
        noticeDetailPresenterImp = new NoticeDetailPresenterImp(getBaseActivity());
        noticeDetailPresenterImp.attachView(this);

        if (getArguments() != null) {
            idNotice = getArguments().getInt(Statics.ID_NOTICE);
        }
        photoAdapter = new PhotoAdapter(getBaseActivity());
        commentAdapter = new CommentAdapter(getBaseActivity());

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notice_detail, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
        if (preferenceUtilities.getCurrentIsAdmin() == 1) {
            inflater.inflate(R.menu.menu_detail_notice, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        attachAdapter = new PhotoAdapter(getBaseActivity());
        lvAttach.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        listComment.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));


        noticeDetailPresenterImp.getNoticeDetail(idNotice);
    }

    @OnClick(R.id.fragment_notice_detail_iv_send)
    public void onClickSend() {
        if (!TextUtils.isEmpty(etComment.getText().toString())) {
            noticeDetailPresenterImp.sentComment(noticeDetail.getNoticeNo(), noticeDetail.getRegUserNo(), etComment.getText().toString());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        noticeDetailPresenterImp.detachView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onGetDetailSuccess(final NoticeDetail noticeDetail, boolean isFromCached) {
        this.noticeDetail = noticeDetail;

        showData(noticeDetail);

        if (!isFromCached) {
            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insert(noticeDetail);
                }
            });
        }
    }

    private void showData(final NoticeDetail noticeDetail) {
//            tvDes.configure();
        if (noticeDetail == null) return;
        if (!TextUtils.isEmpty(noticeDetail.getTitle())) {
            tvTitle.setText(Html.fromHtml(noticeDetail.getTitle()));
        } else {
            tvTitle.setText("");
        }

        if (noticeDetail.isImportant()) {
            ivImportant.setVisibility(View.VISIBLE);
        } else {
            ivImportant.setVisibility(View.GONE);
        }

        tvTime.setText(TimeUtils.getTime(noticeDetail.getStartDate()) + " ~ " + TimeUtils.getTime(noticeDetail.getEndDate()));
        photoAdapter.clear();
        lvAttach.setAdapter(photoAdapter);
        photoAdapter.addAll(noticeDetail.getAttaches());
        tvAuthor.setText(noticeDetail.getUserName() + "( " + noticeDetail.getPositionName() + "/" + noticeDetail.getDepartName() + " )");
        photoAdapter.setOnClickItemAttach(new PhotoAdapter.ItemClickListener() {
            @Override
            public void onItemClichAttach(final int position) {
//                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.VIEW_ATTACH);
//                Bundle data = new Bundle();
//                List<Attachments> list = photoAdapter.getItems();
//                data.putString("NAME", noticeDetail.getUserName());
//                data.putString("TIME", noticeDetail.getModDate());
//                data.putInt("POSITION", position);
//                data.putSerializable("LIST_ATTACH", (Serializable) list);
//                MenuEvent event = new MenuEvent();
//                event.setBundle(data);
//                baseEvent.setMenuEvent(event);
//                EventBus.getDefault().post(baseEvent);

                //downLoad Attach
                DownloadAttachDialog dialog = new DownloadAttachDialog(getBaseActivity());
//                String url = "/UI/MobileNotice/DownloadFile.ashx?no=" + String.valueOf(photoAdapter.getItem(position).getAttachNo())
//                        + "&sid=" + CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentMobileSessionId();
                dialog.setNo(photoAdapter.getItem(position).getAttachNo());
                dialog.setNameAttach(photoAdapter.getItem(position).getFileName());
                dialog.show();
            }
        });
        tvDes.setWebViewClient(new WebViewClient());
        tvDes.getSettings().setJavaScriptEnabled(true);
        tvDes.getSettings().setBuiltInZoomControls(true);
        tvDes.getSettings().setLoadWithOverviewMode(true);
//        tvDes.getSettings().setDisplayZoomControls(true);
        String htmlString = noticeDetail.getContent();
        String summary = "<html><body>" + htmlString + "</body></html>";
        tvDes.loadData(summary, "text/html; charset=utf-8", "utf-8");
        listComment.setAdapter(commentAdapter);
        commentAdapter.clear();
        commentAdapter.addAll(noticeDetail.getCommentList());

        commentAdapter.setOnClickLitener(new CommentAdapter.onClickItemListener() {
            @Override
            public void onLongClickItem(final Comment comment) {
                infoCommentDialog = new InfoCommentDialog(getActivity());
                infoCommentDialog.setRegUserNo(comment.getRegUserNo());
                final int commentNo = comment.getCommentNo();
                infoCommentDialog.setOnclickListener(new InfoCommentDialog.onClickInfoListener() {
                    @Override
                    public void deleteListerner() {
                        noticeDetailPresenterImp.deleteComment(commentNo, noticeDetail.getNoticeNo());
                    }

                    @Override
                    public void editListener() {
                        infoCommentDialog.dismiss();
                        EditCommentDialog dialog = new EditCommentDialog(getActivity());
                        dialog.setComment(comment.getContent());
                        dialog.setOnCloseDialogListener(new BaseDialog.OnCloseDialog() {
                            @Override
                            public void onPositive(String newComment) {
                                noticeDetailPresenterImp.editComment(newComment, commentNo, noticeDetail.getNoticeNo());
                            }

                            @Override
                            public void onNegative() {

                            }

                            @Override
                            public void onClose() {

                            }
                        });
                        dialog.show();
                    }

                    @Override
                    public void copyListener() {
                        ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                        cm.setText(comment.getContent());
                        infoCommentDialog.dismiss();
                    }
                });
                infoCommentDialog.show();

            }

            @Override
            public void onClickItem(Comment comment) {

            }
        });
    }

    @Override
    public void onCommentSuccess(String message) {
        Util.hideKeyboard(getActivity());
        noticeDetailPresenterImp.getNoticeDetail(idNotice);
        etComment.setText("");

    }

    @Override
    public void onError(String message) {
        Toast.makeText(getBaseActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteSuccess(String message) {
        infoCommentDialog.dismiss();
        noticeDetailPresenterImp.getNoticeDetail(idNotice);
    }

    @Override
    public void onEditSuccess(String message) {
        infoCommentDialog.dismiss();
        noticeDetailPresenterImp.getNoticeDetail(idNotice);
    }

    @Subscribe
    public void onEvent(DownloadAttachEvent event) {
        if (event != null) {
            no = event.getAttachNo();
            name = event.getName();
            //check permission
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            EXTERNAL_STORAGE_PERMISSION_CONSTANT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                noticeDetailPresenterImp.downloadAttach(event.getAttachNo(), event.getName());
            }


        }

    }

    @Override
    public void onDownloadfileSuccess(String path, String newUrl) {
        File file = new File(path);
        if (file.exists()) {
            Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), "fail", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_CONSTANT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    noticeDetailPresenterImp.downloadAttach(no, name);

                } else {

                    // permission denied, boo! Disable the
                    Toast.makeText(getActivity(), " permission denied, try again!", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
}
