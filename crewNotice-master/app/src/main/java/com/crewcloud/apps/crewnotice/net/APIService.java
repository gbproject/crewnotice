package com.crewcloud.apps.crewnotice.net;

import com.crewcloud.apps.crewnotice.BuildConfig;
import com.crewcloud.apps.crewnotice.CrewCloudApplication;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mb on 3/30/16.
 */
public class APIService {
    private static MyApi instance = null;

    public static MyApi getInstance() {
        if (instance == null) {
            synchronized (MyApi.class) {
                if (instance == null) {
                    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                    httpClient.writeTimeout(15 * 60 * 5000, TimeUnit.MILLISECONDS);
                    httpClient.readTimeout(60 * 5000, TimeUnit.MILLISECONDS);
                    httpClient.connectTimeout(30 * 5000, TimeUnit.MILLISECONDS);
                    OkHttpClient client = httpClient.build();

                    Retrofit retrofit = new Retrofit.Builder()
//                            .baseUrl(BuildConfig.BASE_URL)
                            .baseUrl(CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain())
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                            .addConverterFactory(new ToStringConverterFactory())
                            .client(client)
                            .build();
                    instance = retrofit.create(MyApi.class);
                }
            }
        }

        return instance;
    }
}
