package com.crewcloud.apps.crewnotice.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseAdapter;
import com.crewcloud.apps.crewnotice.data.Selection;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dazone on 1/11/2017.
 */

public class GridAdapterPlus extends BaseAdapter<Selection, GridAdapterPlus.ViewHolder> {

    public GridAdapterPlus(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_plus, parent, false);
        return new GridAdapterPlus.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_plus_iv_icon)
        ImageView ivIcon;

        @Bind(R.id.item_plus_tv_icon)
        TextView tvIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickItemListerner(getAdapterPosition());
                    }
                }
            });
        }

        public void bind(int position) {
            Selection selection = getItem(position);
            tvIcon.setText(selection.getName());
            Picasso.with(mActivity).load(selection.getFile())
                    .into(ivIcon);
        }
    }

    public interface onItemListener {
        void onClickItemListerner(int position);
    }

    private onItemListener listener;


    public void setOnClickListerner(onItemListener listerner) {
        this.listener = listerner;
    }
}
