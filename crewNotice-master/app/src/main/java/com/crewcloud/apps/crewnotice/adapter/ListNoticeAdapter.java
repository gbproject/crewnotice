package com.crewcloud.apps.crewnotice.adapter;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.BaseActivity;
import com.crewcloud.apps.crewnotice.base.BaseAdapter;
import com.crewcloud.apps.crewnotice.base.BaseEvent;
import com.crewcloud.apps.crewnotice.dtos.Notice;
import com.crewcloud.apps.crewnotice.event.MenuEvent;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.view.AutoSizeTextView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tunglam on 12/15/16.
 */

public class ListNoticeAdapter extends BaseAdapter<Notice, ListNoticeAdapter.ViewHolder> {

    private OnClickNewsListener onClickNewsListener;

    public void setOnClickNewsListener(OnClickNewsListener onClickNewsListener) {
        this.onClickNewsListener = onClickNewsListener;
    }

    public ListNoticeAdapter(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public ListNoticeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_notice, parent, false);
        ButterKnife.bind(v);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_list_notice_iv_image)
        ImageView ivImage;

        @Bind(R.id.item_list_notice_tv_content)
        TextView tvContent;

        @Bind(R.id.item_list_notice_tv_author)
        AutoSizeTextView tvAuthor;

        @Bind(R.id.item_list_notice_tv_time)
        TextView tvTime;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickNewsListener != null) {
                        onClickNewsListener.onItemClicked(getAdapterPosition());
                    }
                }
            });
        }

        void bind(int position) {
            final Notice notice = getItem(position);
            if (!TextUtils.isEmpty(notice.getNoticeImg())) {
                ivImage.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + notice.getNoticeImg())
                        .placeholder(R.mipmap.photo_placeholder)
                        .error(R.mipmap.no_photo)
                        .into(ivImage);
            } else {
                ivImage.setVisibility(View.GONE);
            }

            SpannableStringBuilder builder = new SpannableStringBuilder();
            if (Integer.valueOf(notice.getViewCount()) > 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    SpannableString spannableString = new SpannableString(String.valueOf(notice.getTitle()));
                    ClickableSpan clickableTitle = new ClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.NOTICE_DETAIL);
                            Bundle data = new Bundle();
                            data.putInt(Statics.ID_NOTICE, notice.getNoticeNo());
                            MenuEvent menuEvent = new MenuEvent();
                            menuEvent.setBundle(data);
                            baseEvent.setMenuEvent(menuEvent);
                            EventBus.getDefault().post(baseEvent);
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            super.updateDrawState(ds);
                            ds.setUnderlineText(false);
                        }
                    };
                    spannableString.setSpan(clickableTitle, 0, String.valueOf(notice.getTitle()).length(), 0);
                    spannableString.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(android.R.color.black)), 0, String.valueOf(notice.getTitle()).length(), 0);
                    builder.append(spannableString).append("  ")
                            .append(" ", new ImageSpan(getContext(), R.drawable.ic_comment), 0).append(" ");
                    SpannableString commnetSpan = new SpannableString(String.valueOf(notice.getCommentCount()));

                    commnetSpan.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.app_base_color)), 0, String.valueOf(notice.getCommentCount()).length(), 0);
                    builder.append(commnetSpan);

                    if (Integer.valueOf(notice.getViewCount()) > 0) {
                        SpannableStringBuilder stringBuilder = new SpannableStringBuilder().append(" ")
                                .append(" ", new ImageSpan(getContext(), R.drawable.home_ic_view), 0).append(" ");
                        builder.append(stringBuilder);

                        SpannableString viewSpan = new SpannableString(String.valueOf(notice.getViewCount()));
//                        viewSpan.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.app_base_color)), 0,String.valueOf(notice.getCountRead()).length(), 0);

                        ClickableSpan clickableSpan = new ClickableSpan() {
                            @Override
                            public void onClick(View textView) {
                                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.VIEW_USER);
                                Notice dataNotice = new Notice();
                                dataNotice.setViewCount(notice.getViewCount());
                                dataNotice.setNoticeNo(notice.getNoticeNo());

                                Bundle data = new Bundle();
                                data.putSerializable("NOTICE", dataNotice);
                                MenuEvent event = new MenuEvent();
                                event.setBundle(data);
                                baseEvent.setMenuEvent(event);
                                EventBus.getDefault().post(baseEvent);
                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setUnderlineText(false);
                            }
                        };
                        viewSpan.setSpan(clickableSpan, 0, String.valueOf(notice.getViewCount()).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        builder.append(viewSpan);
                    }

                    if (notice.isAttack()) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder().append(" ")
                                .append(" ", new ImageSpan(getContext(), R.drawable.add_attach_ic), 0).append(" ");
                        builder.append(spannableStringBuilder);
                    }

                    if (notice.isImportant()) {
                        SpannableStringBuilder stringBuilder = new SpannableStringBuilder().append(" ")
                                .append(" ", new ImageSpan(getContext(), R.drawable.ic_star_white_18dp), 0).append(" ");
                        builder.append(stringBuilder);
                    }


                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Integer.valueOf(notice.getViewCount()) > 0) {
                        SpannableString spannableString = new SpannableString(String.valueOf(notice.getTitle()));
                        ClickableSpan clickableTitle = new ClickableSpan() {
                            @Override
                            public void onClick(View textView) {
                                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.NOTICE_DETAIL);
                                Bundle data = new Bundle();
                                data.putInt(Statics.ID_NOTICE, notice.getNoticeNo());
                                MenuEvent menuEvent = new MenuEvent();
                                menuEvent.setBundle(data);
                                baseEvent.setMenuEvent(menuEvent);
                                EventBus.getDefault().post(baseEvent);
                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setUnderlineText(false);
                            }
                        };
                        spannableString.setSpan(clickableTitle, 0, String.valueOf(notice.getTitle()).length(), 0);
                        spannableString.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(android.R.color.black)), 0, String.valueOf(notice.getTitle()).length(), 0);
                        builder.append(spannableString).append(" ").append(" ", new ImageSpan(getContext(), R.drawable.home_ic_view), 0).append(" ");
                        SpannableString viewSpan = new SpannableString(String.valueOf(notice.getViewCount()));
                        ClickableSpan clickableSpan = new ClickableSpan() {
                            @Override
                            public void onClick(View textView) {
                                BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.VIEW_USER);
                                Notice dataNotice = new Notice();
                                dataNotice.setViewCount(notice.getViewCount());
                                dataNotice.setNoticeNo(notice.getNoticeNo());

                                Bundle data = new Bundle();
                                data.putSerializable("NOTICE", dataNotice);
                                MenuEvent event = new MenuEvent();
                                event.setBundle(data);
                                baseEvent.setMenuEvent(event);
                                EventBus.getDefault().post(baseEvent);
                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setUnderlineText(false);
                            }
                        };
                        viewSpan.setSpan(clickableSpan, 0, String.valueOf(notice.getViewCount()).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        builder.append(viewSpan);
                    }

                    if (notice.isAttack()) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder().append(" ")
                                .append(" ", new ImageSpan(getContext(), R.drawable.add_attach_ic), 0).append(" ");
                        builder.append(spannableStringBuilder);
                    }

                    if (notice.isImportant()) {
                        SpannableStringBuilder stringBuilder = new SpannableStringBuilder().append(" ")
                                .append(" ", new ImageSpan(getContext(), R.drawable.ic_star_white_18dp), 0).append(" ");
                        builder.append(stringBuilder);
                    }
                }
            }

            if (TextUtils.isEmpty(builder)) {
                tvContent.setText(notice.getTitle());
            } else {
                tvContent.setText(builder);
            }
            tvContent.setMovementMethod(LinkMovementMethod.getInstance());
            tvContent.setHighlightColor(Color.TRANSPARENT);
            tvTime.setText(notice.getRegDate());
//            long time = TimeUtils.getTimeFromString(notice.getRegDate());
//            long timeCreated = TimeUtils.getTimeForMail(time);
//            if (timeCreated == -2) {
//                //today,hh:mm aa
//                tvTime.setText(mActivity.getString(R.string.today) + TimeUtils.displayTimeWithoutOffset(notice.getRegDate(), true));
//            } else {
//                //YYY-MM-DD hh:mm aa
//                tvTime.setText(TimeUtils.displayTimeWithoutOffset(notice.getRegDate(), false));
//            }

//            tvAuthor.setText(notice.getUserName() + "( " + notice.getPositionName() + ",/ " + notice.getDepartName() + " ) ");
            tvAuthor.setText(notice.getContent());


        }
    }

    public interface OnClickNewsListener {
        void onItemClicked(int position);
    }
}
