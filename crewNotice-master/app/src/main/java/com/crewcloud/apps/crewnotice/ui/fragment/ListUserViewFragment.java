package com.crewcloud.apps.crewnotice.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.adapter.ViewUserAdapter;
import com.crewcloud.apps.crewnotice.base.BaseEvent;
import com.crewcloud.apps.crewnotice.base.BaseFragment;
import com.crewcloud.apps.crewnotice.data.UserViewNotice;
import com.crewcloud.apps.crewnotice.dtos.Notice;
import com.crewcloud.apps.crewnotice.event.MenuEvent;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.module.userview.UserViewNoticePresenter;
import com.crewcloud.apps.crewnotice.module.userview.UserViewNoticePresenterImp;
import com.crewcloud.apps.crewnotice.util.Util;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dazone on 1/9/2017.
 */

public class ListUserViewFragment extends BaseFragment implements UserViewNoticePresenter.view {

    @Bind(R.id.list_user_view)
    RecyclerView recyclerView;

    ViewUserAdapter adapter;

    UserViewNoticePresenterImp userViewNoticePresenterImp;
    private int noticeNo;
    private int countRead = 0;
    boolean isCheck = false;
    private List<UserViewNotice> lstUser;


    public static BaseFragment newInstance(Bundle bundle) {
        ListUserViewFragment listUserViewFragment = new ListUserViewFragment();
        listUserViewFragment.setArguments(bundle);
        return listUserViewFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {
            Notice notice = (Notice) getArguments().getSerializable("NOTICE");
            if (notice != null) {
                noticeNo = notice.getNoticeNo();
//                countRead = notice.getViewUserCnt();
            }
        }
        userViewNoticePresenterImp = new UserViewNoticePresenterImp(getBaseActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_user_view, container, false);
        ButterKnife.bind(this, view);
        adapter = new ViewUserAdapter(getBaseActivity());
        setTitle(getString(R.string.list_view));
        userViewNoticePresenterImp.attachView(this);
        lstUser = new ArrayList<>();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_view_list_user, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        adapter.setOnMailItemSelectListener(new ViewUserAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(UserViewNotice userViewNotice) {
                if (userViewNotice.ismIsSelected()) {
                    lstUser.add(userViewNotice);
                    setTitle(lstUser.size() + "/" + countRead);
                } else {
                    lstUser.remove(userViewNotice);
                    if (lstUser.size() > 0) {
                        setTitle(lstUser.size() + "/" + countRead);
                    } else {
                        setTitle(getString(R.string.list_view));
                    }
                }

            }
        });
        userViewNoticePresenterImp.getUserView(noticeNo);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_list_check_all:
                if (isCheck) {
                    isCheck = false;
                    releaseAllSelectedItem();
                } else {
                    selectAllListUser();
                    isCheck = true;
                }
                break;
            case R.id.menu_list_sent_email:

                if (lstUser.size() > 0) {
                    BaseEvent baseEvent = new BaseEvent(BaseEvent.EventType.SENT_EMAIL);
                    Bundle data = new Bundle();
                    data.putSerializable(Statics.USER_VIEW_NOTICE, (Serializable) lstUser);
                    MenuEvent event = new MenuEvent();
                    event.setBundle(data);
                    baseEvent.setMenuEvent(event);
                    EventBus.getDefault().post(baseEvent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity())
                            .setTitle(R.string.app_name)
                            .setMessage(R.string.please_select_user)
                            .setPositiveButton(Util.getString(R.string.string_ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
                break;
        }
        return false;
    }

    private void releaseAllSelectedItem() {
        int index = 0;
        lstUser.clear();
        for (UserViewNotice userViewNotice : adapter.getItems()) {
            if (userViewNotice.ismIsSelected()) {
                userViewNotice.setmIsSelected(false);
                lstUser.add(userViewNotice);
            }
            index++;

        }
        setTitle(getString(R.string.list_view));
        adapter.notifyDataSetChanged();
    }

    private void selectAllListUser() {
        int index = 0;
        lstUser.clear();
        for (UserViewNotice userViewNotice : adapter.getItems()) {
            if (!userViewNotice.ismIsSelected()) {
                userViewNotice.setmIsSelected(true);
                lstUser.add(userViewNotice);
            }
            index++;
        }
        setTitle("" + countRead + "/" + countRead);
        adapter.notifyDataSetChanged();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        userViewNoticePresenterImp.detachView();
    }


    @Override
    public void onGetUserViewSuccess(List<UserViewNotice> userViewNotice) {

        adapter.addAll(userViewNotice);

    }

    @Override
    public void onError(String error) {

    }
}


