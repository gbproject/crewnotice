package com.crewcloud.apps.crewnotice.dtos;


import com.crewcloud.apps.crewnotice.CrewCloudApplication;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDto implements Serializable {

    @SerializedName("Id")
    public int Id;

    @SerializedName("CompanyNo")
    public int CompanyNo;

    @SerializedName("PermissionType")
    public int PermissionType;//0 normal, 1 admin

    @SerializedName("userID")
    public String userID;

    @SerializedName("FullName")
    public String FullName;

    @SerializedName("MailAddress")
    public String MailAddress;

    @SerializedName("session")
    public String session;

    @SerializedName("avatar")
    public String avatar;

    @SerializedName("nameCompany")
    public String NameCompany;

    private String time;


    public PreferenceUtilities prefs = CrewCloudApplication.getInstance().getPreferenceUtilities();

    public UserDto() {
        prefs = CrewCloudApplication.getInstance().getPreferenceUtilities();
    }

    public UserDto(int permissionType, String userID, String fullName, String mailAddress, String time, String avatar) {
        PermissionType = permissionType;
        this.userID = userID;
        FullName = fullName;
        this.time = time;
        MailAddress = mailAddress;
        this.avatar = avatar;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCompanyNo() {
        return CompanyNo;
    }

    public void setCompanyNo(int companyNo) {
        CompanyNo = companyNo;
    }

    public int getPermissionType() {
        return PermissionType;
    }

    public void setPermissionType(int permissionType) {
        PermissionType = permissionType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getMailAddress() {
        return MailAddress;
    }

    public void setMailAddress(String mailAddress) {
        MailAddress = mailAddress;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNameCompany() {
        return NameCompany;
    }

    public void setNameCompany(String nameCompany) {
        NameCompany = nameCompany;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
