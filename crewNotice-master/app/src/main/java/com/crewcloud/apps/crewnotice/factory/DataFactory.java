package com.crewcloud.apps.crewnotice.factory;

import android.app.Activity;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.data.Attachments;
import com.crewcloud.apps.crewnotice.data.NoticeDetail;
import com.crewcloud.apps.crewnotice.data.Selection;
import com.crewcloud.apps.crewnotice.dtos.Notice;
import com.crewcloud.apps.crewnotice.dtos.UserDto;
import com.crewcloud.apps.crewnotice.event.MenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tunglam on 12/15/16.
 */

public class DataFactory {
    private static NoticeDetail detail;

//    public static List<Notice> getListNotice() {
//        List<Notice> newsInfoList = new ArrayList<>();
//
//        Notice notice = new Notice();
//        notice.setPositionName("Designer");
//        notice.setUserName("Tran Dao Manh");
//        notice.setViewCount("12");
//        notice.setMesCount(2);
//        notice.setTitle("Title 1");
//        notice.setNoticeNo(94);
//
//        Notice notice1 = new Notice();
//        notice.setPositionName("DEV");
//        notice.setUserName("TUNG LAM");
//        notice.setViewCount("14");
//        notice.setMesCount(0);
//        notice.setTitle("Title 2");
//        notice.setNoticeNo(94);
//
//        Notice notice2 = new Notice();
//        notice.setPositionName("Developer");
//        notice.setUserName("NGOC TRINH");
//        notice.setViewCount("1");
//        notice.setMesCount(4);
//        notice.setTitle("Title 3");
//        notice.setNoticeNo(94);
//
//        newsInfoList.add(notice);
//        newsInfoList.add(notice1);
//        newsInfoList.add(notice2);
//
//        return newsInfoList;
//    }

    public static List<MenuItem> getMenuItem() {
        List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(new MenuItem(1, "Menu 1"));
        menuItems.add(new MenuItem(2, "Menu 2"));
        menuItems.add(new MenuItem(3, "Menu 3"));
        menuItems.add(new MenuItem(4, "Menu 4"));
        menuItems.add(new MenuItem(4, "Menu 5"));
        return menuItems;
    }

    public static List<Selection> getMenuSelection(Activity activity) {
        List<Selection> menuSelections = new ArrayList<>();
        menuSelections.add(new Selection(activity.getString(R.string.camera), R.drawable.attach_ic_camera));
        menuSelections.add(new Selection(activity.getString(R.string.picture), R.drawable.attach_ic_images));
        menuSelections.add(new Selection(activity.getString(R.string.video), R.drawable.attach_ic_video));
        menuSelections.add(new Selection(activity.getString(R.string.audio), R.drawable.attach_ic_video_record));
        menuSelections.add(new Selection(activity.getString(R.string.file), R.drawable.attach_ic_file));
        return menuSelections;
    }

    public static List<UserDto> getUser() {
        List<UserDto> userDtos = new ArrayList<>();
        userDtos.add(new UserDto(1, "1", "NGUYEN NGO GIAP1", "General Management Div.", "2016/01/02 02:00 pm", ""));
        userDtos.add(new UserDto(1, "1", "NGUYEN NGO GIAP2", "General Management Div.", "2016/01/02 03:00 pm", ""));
        userDtos.add(new UserDto(1, "1", "NGUYEN NGO GIAP3", "General Management Div.", "2016/01/02 04:00 pm", ""));
        userDtos.add(new UserDto(1, "1", "NGUYEN NGO GIAP4", "General Management Div.", "2016/01/02 05:00 pm", ""));
        return userDtos;
    }

//    public static Notice getNotice() {
//        Notice notice = new Notice();
//        notice.setId("1");
//        notice.setAuthor("Tran Dao Manh");
//        notice.setContent("Annotations on the interface methods and its parameters indicate how a request will be handled.");
//        notice.setNumber_comment("3");
//        notice.setTime("today, 2:43 pm");
//        return notice;
//    }

    public static List<Attachments> getPhoto() {
        List<Attachments> lstPhoto = new ArrayList<>();
        lstPhoto.add(new Attachments("ngoctrinh", "http://photos.thegioisao.net/anh-dep/chan-dung/ngoc-trinh-08-6332-1434679400.jpg"));
        lstPhoto.add(new Attachments("ngoctrinh2", "http://www.xaluan.com/images/news/Image/2015/08/27/nu-hoang-noi-y-ngoc-trinh-tu-khai-guong-mat-dao-keo-hinh-4.jpg"));
        lstPhoto.add(new Attachments("ao dai", "http://kenh14cdn.com/thumb_w/600/2016/5-1454435948579.jpg"));
        return lstPhoto;
    }

//    public static NoticeDetail getDetail() {
//        NoticeDetail detail = new NoticeDetail();
//        List<Attachments> lstAttach = new ArrayList<>();
//
//        Attachments attachments = new Attachments();
//        attachments.setDownloadUrl("R.drawable.android");
//        attachments.setFileName("ngoctrinh.jpg");
//        Attachments attachments1 = new Attachments();
//        attachments1.setDownloadUrl("R.drawable.excel");
//        attachments1.setFileName("ngoctrinh1.jpg");
//        lstAttach.add(attachments1);
//        lstAttach.add(attachments);
//        detail.setAttaches(lstAttach);
//        detail.setTitle("TITLE");
//        detail.setUserName("TRAN DAO MANH");
//        detail.setPositionName("DEVELOPER");
//        return detail;
//    }
}
