package com.crewcloud.apps.crewnotice.net;

/**
 * Created by tunglam on 12/23/16.
 */
public class BodyRequest {
    private String timeZoneOffset;
    private String languageCode;
    private String sessionId;

    private int divisionNo;
    private String searchText;
    private boolean importantOnly;
    private String countOfArticles;
    private int anchorNoticeNo;
    private int noticeNo;
    private int regUserNo;
    private String Comment;
    private int commentNo;
    private int modUserNo;

    public BodyRequest(String timeZoneOffset, String languageCode, String sessionId) {
        this.timeZoneOffset = timeZoneOffset;
        this.languageCode = languageCode;
        this.sessionId = sessionId;
    }

    public BodyRequest(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setContent(String content) {
        this.Comment = content;
    }

    public void setRegUserNo(int regUserNo) {
        this.regUserNo = regUserNo;
    }

    public void setNoticeNo(int noticeNo) {
        this.noticeNo = noticeNo;
    }

    public void setAnchorNoticeNo(int anchorNoticeNo) {
        this.anchorNoticeNo = anchorNoticeNo;
    }

    public void setCountOfArticles(String countOfArticles) {
        this.countOfArticles = countOfArticles;
    }

    public void setImportantOnly(boolean importantOnly) {
        this.importantOnly = importantOnly;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setDivisionNo(int divisionNo) {
        this.divisionNo = divisionNo;
    }

    public void setCommentNo(int commentNo) {
        this.commentNo = commentNo;
    }

    public void setModUserNo(int modUserNo) {
        this.modUserNo = modUserNo;
    }
}
