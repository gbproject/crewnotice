package com.crewcloud.apps.crewnotice.module.userview;

import com.crewcloud.apps.crewnotice.base.BaseView;
import com.crewcloud.apps.crewnotice.data.UserViewNotice;

import java.util.List;

/**
 * Created by Dazone on 1/12/2017.
 */

public interface UserViewNoticePresenter {
    interface view extends BaseView {
        void onGetUserViewSuccess(List<UserViewNotice> userViewNotice);

        void onError(String error);
    }

    interface presenter {
        void getUserView(int noticeNo);
    }
}
