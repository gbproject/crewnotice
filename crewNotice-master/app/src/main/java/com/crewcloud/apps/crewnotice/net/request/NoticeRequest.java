package com.crewcloud.apps.crewnotice.net.request;

/**
 * Created by dazone on 4/18/2017.
 */

public class NoticeRequest {
    private String sessionId;
    private String searchText;
    private int countOfArticles;
    private int anchorNoticeNo;
    private int divisionNo;
    private String languageCode;
    private String timeZoneOffset;
    private boolean importantOnly;

    public NoticeRequest(String sessionId, String languageCode, String timeZoneOffset) {
        this.sessionId = sessionId;
        this.languageCode = languageCode;
        this.timeZoneOffset = timeZoneOffset;
    }

    public NoticeRequest() {
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setCountOfArticles(int countOfArticles) {
        this.countOfArticles = countOfArticles;
    }

    public void setAnchorNoticeNo(int anchorNoticeNo) {
        this.anchorNoticeNo = anchorNoticeNo;
    }

    public void setDivisionNo(int divisionNo) {
        this.divisionNo = divisionNo;
    }

    public void setImportantOnly(boolean importantOnly) {
        this.importantOnly = importantOnly;
    }
}
