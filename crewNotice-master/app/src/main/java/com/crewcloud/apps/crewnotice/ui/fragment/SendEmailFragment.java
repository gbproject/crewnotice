package com.crewcloud.apps.crewnotice.ui.fragment;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.crewcloud.apps.crewnotice.R;
import com.crewcloud.apps.crewnotice.base.BaseFragment;
import com.crewcloud.apps.crewnotice.data.UserViewNotice;
import com.crewcloud.apps.crewnotice.loginv2.Statics;
import com.crewcloud.apps.crewnotice.util.PreferenceUtilities;
import com.crewcloud.apps.crewnotice.view.PersonCompleteView;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dazone on 1/13/2017.
 */

public class SendEmailFragment extends BaseFragment {

    @Bind(R.id.fragment_sent_email_sp_mail_from)
    TextView tvMailFrom;

    @Bind(R.id.fragment_sent_email_tcv_to)
    PersonCompleteView tcvTo;

    @Bind(R.id.fragment_sent_email_et_subject)
    EditText etSubject;

    @Bind(R.id.fragment_sent_email_et_compose_email)
    EditText etComposeEmail;

    //    EmailFromAdapter adater;
    FilteredArrayAdapter adapter;

    List<UserViewNotice> lstUserView = new ArrayList<>();


    public static BaseFragment newInstance(Bundle bundle) {
        SendEmailFragment sendEmailFragment = new SendEmailFragment();
        sendEmailFragment.setArguments(bundle);
        return sendEmailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.compose_email));
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            lstUserView = (List<UserViewNotice>) getArguments().getSerializable(Statics.USER_VIEW_NOTICE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sent_email, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tcvTo.setType(0);
        adapter = new FilteredArrayAdapter<UserViewNotice>(getActivity(), R.layout.person_layout, lstUserView) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {

                    LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = l.inflate(R.layout.person_layout, parent, false);
                }
                UserViewNotice p = getItem(position);
                if (p != null) {
                    ((TextView) convertView.findViewById(R.id.name)).setText(p.getUserName());
                    ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());
                }
                return convertView;
            }

            @Override
            protected boolean keepObject(UserViewNotice userViewNotice, String mask) {
                mask = mask.toLowerCase();
                return userViewNotice.getUserName().toLowerCase().contains(mask) || userViewNotice.getUserName().toLowerCase().contains(mask);
            }
        };
        tcvTo.setAdapter(adapter);
        tcvTo.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        initData();
    }

    private void initData() {
        PreferenceUtilities preferenceUtilities = new PreferenceUtilities();
        String email = preferenceUtilities.getEmail();
        String userName = preferenceUtilities.getFullName();
        tvMailFrom.setText(userName + "<" + email + ">");

        for (UserViewNotice userViewNotice : lstUserView) {
            tcvTo.addObject(userViewNotice);
        }
        tcvTo.setTokenListener(new TokenCompleteTextView.TokenListener<UserViewNotice>() {
            @Override
            public void onTokenAdded(UserViewNotice token) {

            }

            @Override
            public void onTokenRemoved(UserViewNotice token) {

            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_send, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.fragment_sent_email_ib_dropdown)
    public void onClickDropdown() {

    }

    @OnClick(R.id.fragment_sent_email_ib_attach)
    public void onClickAttach() {
        Intent i = new Intent(getActivity(), FilePickerActivity.class);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, Statics.FILE_PICKER_SELECT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Statics.FILE_PICKER_SELECT:
                    if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                        // For JellyBean and above
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = data.getClipData();

                            if (clip != null) {
                                for (int i = 0; i < clip.getItemCount(); i++) {
                                    Uri uri = clip.getItemAt(i).getUri();
//                                    GetFile(uri);
                                    // Do something with the URI
                                }
                            }
                            // For Ice Cream Sandwich
                        } else {
                            ArrayList<String> paths = data.getStringArrayListExtra
                                    (FilePickerActivity.EXTRA_PATHS);

                            if (paths != null) {
                                for (String path : paths) {
                                    Uri uri = Uri.parse(path);
                                    // Do something with the URI
//                                    GetFile(uri);
                                }
                            }
                        }

                    } else {
                        Uri uri = data.getData();
//                        GetFile(uri);
                        // Do something with the URI
                    }
                    break;
            }
        }
    }
}
